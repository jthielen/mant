function [] = jt_topoplot(w, cfg)
% [] = jt_topoplot(w, cfg)
% Plot topoplot.
%
% INPUT
%   w   = [n 1]    the weights
%   cfg = [struct] configuration structure
%       .capfile    = [str] cap file (cap64.loc)
%       .electrodes = [str] how to depict the electrode (on)
%       .style      = [str] how to depict the head (map)
%       .headcolor  = [str] color of the head ('k')
%       .markersize = [str] marker size ([]) ([] is the default and  
%                     dependent on the number of channels plotted)

% Defaults
if nargin < 2 || isempty(cfg); cfg = []; end
capfile = jt_parse_cfg(cfg, 'capfile', 'nt_cap64.loc');
electrodes = jt_parse_cfg(cfg, 'electrodes', 'on');
style = jt_parse_cfg(cfg, 'style', 'map');
headcolor = jt_parse_cfg(cfg, 'headcolor', 'k');
markersize = jt_parse_cfg(cfg, 'markersize', []);

% Replace NaN with zero
w(isnan(w)) = 0;

% Check extension
[~, file, ext] = fileparts(capfile);
if ~strcmpi(ext, 'loc')
    capfile = [file '.loc'];
end

% Check cap existance
if ~exist(capfile, 'file')
    error('Capfile not found on the path: %s.', capfile);
end

% Plot
topoplot(w, capfile, ...
    'electrodes', electrodes, ...
    'style', style, ...
    'verbose', 'off', ...
    'colormap', ikelvin, ...
    'hcolor', headcolor, ...
    'emarker', {'.', 'k', markersize, 1});

function [c] = ikelvin(m, w)

    if nargin < 1 || isempty(m) 
        m = size(get(gcf, 'colormap'), 1); 
    end
    if nargin < 2 || isempty(w) 
        w = [.1 .3]; 
    end
    if numel(w)==1 
        w = [0.5 * w w + 0.5 * (0.5 - w)]; 
    end

    %  pos     hue   sat   value
    cu = [
       0.0     1/2   0.0   1.0
       w(1)    1/2   0.6   0.95
       w(2)    2/3   1.0   0.8
       0.5     2/3   1.0   0.3
    ];

    cl = cu;
    cl(:, 3:4) = cl(end:-1:1, 3:4);
    cl(:, 2)   = cl(:, 2) - 0.5;
    cu(:, 1)   = 1 - cu(end:-1:1, 1);

    x = linspace(0, 1, m)';
    l = (x < 0.5); 
    u = ~l;
    for i = 1:3
        h(l, i) = interp1(cl(:, 1), cl(:, i + 1), x(l));
        h(u, i) = interp1(cu(:, 1), cu(:, i + 1), x(u));
    end
    h = flipud(h);
    c = hsv2rgb(h);
