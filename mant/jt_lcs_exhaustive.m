function [subset,value] = jt_lcs_exhaustive(x, k)
% [subset, value] = jt_lcs_exhaustive(x, k)
% Finds the least correlating subset in an exhaustive manner.
%
% INPUT
%   x = [m n] m samples of n variables
%   k = [int] number of variables in subset
%
% OUTPUT
%   subset = [1 k] positions of the subset in x
%   value  = [flt] correlation value of the subset

num = size(x, 2);
if k > num; error('x does not hold n variables!'); end

% Compute correlations
rho = jt_correlation(x, x);
rho = max(rho, [], 3);
rho(logical(eye(num))) = NaN; %ignore diagonal

% Compute all combinations
combinations = combnk(1:num, k);

% Compute LCS
subset = [];
value = Inf;
for i = 1:size(combinations, 1)
    tmp = nanmax(nanmax(rho(combinations(i, :), combinations(i, :))));
    if tmp < value
        value = tmp;
        subset = combinations(i, :);
    end
end
