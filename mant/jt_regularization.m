function [lambda] = jt_regularization(lambda, N, intercept, maximum, alpha)
% [lambda] = jt_regularization(lambda, N, intercept, maximum, alpha)
%
% INPUT
%   lambda    = [flt] between 0 and 1 meaning heavy to no regularization
%             = [str] taper function, see jt_make_taper
%   N         = [n 1] size of the to be regularized item
%   intercept = [int] adds zero regularization (useful for an intercept)
%   maximum   = [flt] maximum regularization on the covariance
%   alpha     = [int] relative parts of the taper that is regularized
% 
% OUTPUT
%   lambda = [n] the regularization penalties

if nargin < 3 || isempty(intercept); intercept = 0; end
if nargin < 4 || isempty(maximum); maximum = 1; end
if nargin < 5 || isempty(alpha); alpha = .2; end

% Design taper for regularization
if ischar(lambda)
    if intercept
        lambda = cat(1, jt_make_taper(lambda, N(1:end-1), 1, alpha), ones(N(end), 1)); 
    else
        lambda = jt_make_taper(lambda, N, maximum, alpha); 
    end
else
    if numel(lambda)==1
        lambda = maximum*repmat(lambda, sum(N), 1);
    end
end

% Revert order (i.e., 1 confidence means no (i.e., low) penalty and vice versa)
lambda = max(min((maximum-lambda), maximum), 0);

function [T] = jt_make_taper(type, len, amp, perc)
    % [T] = jt_make_taper(type, len, amp, perc)
    %
    % INPUT
    %   type  = [str] type of taper ('revtukey')
    %   len   = [1 n] lengths of n tapers (100)
    %   amp   = [1 n] amplifier for each taper (1)
    %   perc  = [flt] part of window to control (.2)
    %
    % OUTPUT
    %   T = [sum(L) 1]

    % Defaults
    if nargin < 1 || isempty(type); type = 'tukey'; end
    if nargin < 2 || isempty(len); len = 100; end
    if nargin < 3 || isempty(type); amp = 1; end
    if nargin < 4 || isempty(perc); perc = .2; end

    % Check input
    if numel(amp)~=numel(len); amp=amp*ones(1,numel(len)); end

    % Make taper
    switch type
        case 'hanning'
            T = [];
            for i = 1:numel(len)
                T = cat(1, T, amp(i) * (hanning(len(i))));
            end
        case 'tukey'
            T = [];
            for i = 1:numel(len)
                T = cat(1, T, amp(i) * (tukeywin(len(i), perc)));
            end
        case 'revhanning'
            T = [];
            for i = 1:numel(len)
                T = cat(1, T, amp(i) * (1 - hanning(len(i))));
            end
        case 'revtukey'
            T = [];
            for i = 1:numel(len)
                T = cat(1, T, amp(i) * (1 - tukeywin(len(i), perc)));
            end
        otherwise
            error('Unknown taper: %s.', type);
    end
    