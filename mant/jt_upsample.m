function [x] = jt_upsample(x, rate, dim)
% [x] = jt_upsample(x, rate, dim)
% Upsamples an array or matrix with a certain rate directly.
%
% INPUT
%   x    = [N-D]  matrix to be upsampled
%        = [cell] cell to be upsampled
%   rate = [int]  rate at which to upsample 
%   dim  = [int]  dimension along which to upsample (1)
% 
% OUTPUT
%   v = [N-D]  upsampled matrix
%       [cell] cell with individual upsampled variables

if nargin < 3 || isempty(dim); dim = 1; end

% Variables
[r, c] = size(x);

% Upsample
if iscell(x)
    for i = 1:numel(x)
        x{i} = jt_upsample(x{i}, rate, dim);
    end
else
    if dim == 1
        x = reshape(permute(repmat(x, [1 1 rate]), [3 1 2]), rate * r, c);
    else
        x = reshape(permute(repmat(x, [1 1 rate]), [1 3 2]), r, rate * c);
    end
end
