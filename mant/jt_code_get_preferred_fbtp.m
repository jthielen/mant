function [pfbtp] = jt_code_get_preferred_fbtp(m, check, verbosity)
% [pfbtp] = jt_code_get_preferred_fbtp(m, check, verbosity)
% Generates the entire set of Preferred-Pair of Feedback Tap Positions.
%
% INPUT
%   m         = [int] degree of polynomial
%   check     = [str] whether or not to double-check for 3-valued correlation (0)
%   verbosity = [int] verbosity level (0)
%
% OUTPUT
%   pfbtp = [cell] cell array of all combinations of preferred pair of 
%                  primitive polynomials of degree m.

if nargin < 2 || isempty(check); check = 0; end
if nargin < 3 || isempty(verbosity); verbosity = 0; end

% Initialize cell array of preferred Feedback Tap Positions
pfbtp = {};

% Find all Feedback Tap Positions for degree m
fbtp = jt_code_get_fbtp(m);

% Find all combinations of fbtp
cmb = nchoosek(1:numel(fbtp), 2);
if isempty(cmb)
    return 
end

% Find all combinations that form a preferred pair
for i = 1:size(cmb, 1)
    if jt_code_is_preferred_pair(m, fbtp{cmb(i, 1)}, fbtp{cmb(i, 2)})
        pfbtp{end+1, 1} = fbtp{cmb(i, 1)};
        pfbtp{end  , 2} = fbtp{cmb(i, 2)};
    end
end

% Check 3-valued cross-correlations also
n_pfbtp = size(pfbtp, 1);
if check > 0 && n_pfbtp > 0
    flags = zeros(1, n_pfbtp);
    for i = 1:n_pfbtp
        codes = jt_code_make_gold_code(m, pfbtp{i, 1}, pfbtp{i, 2});
        flags(i) = jt_code_check_crosscorrelation(m, codes);
    end
    if any(~flags)
        fprintf('Warning: deleted %d pfbtp!!', sum(~flags))
        pfbtp = pfbtp(flags==1, :);
    end
end

% Print
if verbosity > 0
    for i = 1:size(pfbtp, 1)
        for k = 1:2
            fprintf('[')
            for j = 1:numel(pfbtp{i, k})
                fprintf([num2str(pfbtp{i, k}(j)) ' '])
            end
            fprintf('\b] ');
        end
        fprintf('\n')
    end
    fprintf('\n')
end
