function [time] = jt_stopping_static(data, cfg)
% [time] = jt_stopping_static(X, y, T, cfg)
% Learn stopping time for static stopping.
%
% INPUT
%   data = [struct] data structure:
%       .X   = [c m k] training data of c channels, m samples, and k trials
%       .y   = [k 1]   training labels or targets of k trials, i.e., indexes in V
%       .V   = [s p]   one period of training sequences of s samples and p codes, should be at the same sample frequency as .X
%   cfg = [struct] classifier configuration structure, see jt_tmc_train
%
% OUTPUT
%   time = [flt] time at which to stop

data.U = data.V;
method = cfg.method;
cfg.method = 'fix';
cfg.verbosity = 0;
cfg.subset_U = cfg.subset_V;
cfg.layout_U = cfg.layout_V;

% Fold data chronologically
n_trials = size(data.X, 3);
cv = repmat(1:cfg.n_folds, ceil(n_trials / cfg.n_folds), 1)';
cv = sort(cv(1:n_trials));

% Segments
segments = cfg.segment_time:cfg.segment_time:cfg.max_time;
n_segments = numel(segments);

% Loop over folds
accuracy = zeros(cfg.n_folds, n_segments);
for i_fold = 1:cfg.n_folds

    % Assign folds
    trnidx = cv ~= i_fold;
    tstidx = cv == i_fold;

    % Train classifier
    classifier = jt_tmc_train(struct('X', data.X(:, :, trnidx), 'y', data.y(trnidx), 'V', data.V, 'U', data.V), cfg);

    % Loop over segments
    for i_segment = 1:n_segments
    
        % Make sure the max time (at which to stop) is correct
        classifier.cfg.max_time = i_segment * cfg.segment_time;
    
        % Apply classifier
        labels = jt_tmc_apply(classifier, data.X(:, 1:floor(i_segment * cfg.segment_time * cfg.fs), tstidx));
        accuracy(i_fold, i_segment) = mean(labels == data.y(tstidx));
    end
end
accuracy = mean(accuracy, 1);

% Find timing
switch method
    case 'static_max_itr'
        [~, idx] = max(jt_itr(size(data.V, 2), accuracy, segments));
    case 'static_max_acc'
        [~, idx] = max(accuracy);
    case 'static_tgt_acc'
        idx = find(cumsum(accuracy >= cfg.accuracy) >= cfg.patience, 1);
        if isempty(idx)
            [~, idx] = max(accuracy);
        end
    otherwise
        error('Unknown method: %s', method);
end
time = segments(idx);
