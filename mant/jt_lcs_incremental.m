function [subset, value] = jt_lcs_incremental(x, k)
% [subset, value] = jt_lcs_incremental(x, k)
% Finds the least correlating subset in an incremental manner.
%
%First the least correlating couple is found. At each following the element
%that correlates the least with the current subset is added.
%
% INPUT
%   x = [m n] m samples of n variables
%   k = [int] number of variables in subset
%
% OUTPUT
%   subset = [1 k] positions of the subset in x
%   value  = [flt] correlation value of the subset

num = size(x, 2);
if k > num; error('x does not hold n variables!'); end

% Compute correlations
rho = jt_correlation(x, x);
rho = max(rho, [], 3);
rho(logical(eye(num))) = NaN; %ignore diagonal

% Determine first element
[~, p] = nanmin(rho(:));
[el1, el2] = ind2sub(size(rho), p);
subset = [el1, el2];

% Determine entire LCS
if k > 2
    stack = find(~ismember(1:num, subset));
    for i = 1:k - 2
        matrix = rho(stack, subset);
        [~, p] = nanmin(nanmax(matrix), [], 2);
        [r, ~] = ind2sub(size(matrix), p);
        subset = cat(2, subset, stack(r));
        stack  = find(~ismember(1:num, subset)); 
    end
end

% Compute output
subset = sort(subset, 'ascend');
value = nanmax(nanmax(rho(subset, subset)));
