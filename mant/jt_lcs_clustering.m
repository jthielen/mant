function [subset, value] = jt_lcs_clustering(x, k)
% [subset, value] = jt_lcs_clustering(v, k)
% Finds the least correlating subset in an clustering manner.
%
% INPUT
%   x = [m n] m samples of n variables
%   k = [int] number of variables in subset
%
% OUTPUT
%   subset = [1 k] ascending indices of the subset in v
%   value  = [flt] correlation value of the subset

p = size(x, 2);

% Compute correlation
c = jt_correlation(x, x);

% Cluster
links = linkage(c, 'single');
clusters = cluster(links, 'maxclust', k)';

% Ignore diagonal
c(logical(eye(p))) = NaN;

% Define order of clusters
r = zeros(1, k);
for i = 1:k
    % Select cluster
    clust = clusters == i;
    % Select highest correlation
    r(i) = nanmax(nanmax(c(clust, ~clust), [], 2));
end
[~, order] = sort(r, 'descend');

% Define the subset
rmv = false(1, p);
for i = 1:k
    % Select cluster
    clust = clusters == order(i);
    idx = find(clust);
    % Select representative
    [~, reprs] = nanmin(nanmax(c(clust, ~clust & ~rmv), [], 2));
    reprs = idx(reprs);
    % Remove non-representatives        
    rmv(clust) = true;
    rmv(reprs) = false;
end

% Extract subset
subset = find(~rmv);

% Compute output
subset = sort(subset, 'ascend');
value = nanmax(nanmax(c(subset, subset)));
