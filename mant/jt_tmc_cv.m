function [results] = jt_tmc_cv(data, cfg, n_folds)
% [results] = jt_tmc_cv(data, cfg, n_folds)
% Cross-validation to validate classification performance.
%
% INPUT
%   data = [struct] data structure:
%       .X   = [c m k]  data of c channels, m samples and k trials
%       .y   = [1 k]    labels of k trials
%       .V   = [s p]    trained sequences of s samples and p codes
%   cfg      = [struct] classifier configuration structure, see jt_tmc_train
%   n_folds  = [int]    number of folds (10)
%
% OUTPUT
%   results = [struct] results structure
%       .accuracy = [1 n] accuracies for each fold
%       .time = [1 n] trial-lengths for each fold

% Defaults
if nargin < 2 || isempty(cfg); cfg = []; end
if nargin < 3 || isempty(n_folds); n_folds = 10; end
cfg.verbosity = 0;
cfg.subset_U = cfg.subset_V;
cfg.layout_U = cfg.layout_V;

% Fold data chronologically
n_trials = size(data.X, 3);
cv = repmat(1:n_folds, ceil(n_trials / n_folds), 1)';
cv = sort(cv(1:n_trials));

% Loop over folds
results.accuracy = zeros(1, n_folds);
results.time = zeros(1, n_folds);
for i_fold = 1:n_folds

    % Assign folds
    trnidx = cv ~= i_fold;
    tstidx = cv == i_fold;

    % Train classifier
    classifier = jt_tmc_train(struct('X', data.X(:, :, trnidx), 'y', data.y(trnidx), 'V', data.V, 'U', data.V), cfg);
    
    % Apply classifier
    [label, result] = jt_tmc_apply(classifier, data.X(:, :, tstidx));
    results.accuracy(i_fold) = mean(label == data.y(tstidx));
    results.time(i_fold) = mean(result.time);
end
