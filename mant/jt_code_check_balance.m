function [flags, balance] = jt_code_check_balance(codes, tol)
% [flags, balance] = jt_checkbalance(codes, tol)
% Determines the number of ones and zeros in a variable and checks if these 
% differ. Balanced means equal number of ones and zeros.
%
% INPUT
%   codes = [m n] codes of shape bits by codes
%   tol   = [int] difference between zeros and ones that is allowed (0)
% 
% OUTPUT:
%   flags   = [1 n] 1 if balanced, 0 if not
%   balance = [2 n] amount of ones and zeros for all codes

if nargin < 2; tol = 0; end

% Compute balance
balance = [sum(codes, 1); sum(~codes, 1)];

% Check balance
flags = abs(balance(1, :) - balance(2, :)) <= tol;
