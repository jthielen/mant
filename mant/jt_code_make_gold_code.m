function [codes] = jt_code_make_gold_code(m, v, w, addmls)
% [codes] = jt_code_make_gold_code(m, v, w, addmls)
% Generates Gold codes.
% 
% INPUT
%   m      = [int] register length (6)
%   v      = [1 p] array of p feedback tab points ([6 1])
%   w      = [1 q] array of q feedback tab points ([6 5 2 1])
%   addmls = [bool] wheter or not to add the m-sequences at the end (true)
% 
% OUTPUT
%   codes = [2^m-1 2^m+1] Gold codes of shape bits by codes
%         = [2^m-1 2^m-1] Gold codes of shape bits by codes (w/o m-sequences)

% Check input
if nargin < 1 || isempty(m); m = 6; end
if nargin < 2 || isempty(v); v = [6 1]; end
if nargin < 3 || isempty(w); w = [6 5 2 1]; end
if nargin < 4 || isempty(addmls); addmls = true; end

% Check if pair of fbtp is a preferred pair
if ~jt_code_is_preferred_pair(m, v, w)
    error('Invalid input: no preferred pair.'); 
end

% Generate two m-sequences
mseq1 = jt_code_make_m_sequence(m, v);
mseq2 = jt_code_make_m_sequence(m, w);

% Generate gold codes
if addmls
    codes = zeros(2^m-1, 2^m+1); 
else
    codes = zeros(2^m-1, 2^m-1); 
end
for i = 1:2^m-1
    codes(:, i) = mod(mseq1 + mseq2, 2);
    mseq2 = circshift(mseq2, -1, 1);
end

% Add the original m-sequences to the set
if addmls
    codes(:, end-1) = mseq1;
    codes(:, end)   = mseq2;
end
