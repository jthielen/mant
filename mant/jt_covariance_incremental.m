function [avg_new, cov_new, m_new] = jt_covariance_incremental(obs, avg_old, cov_old, m_old)
% [avg_new, cov_new, m_new] = jt_covariance_incremental(obs, avg_old, cov_old, m_old)
%
% INPUT
%   obs     = [m n] new piece of data of shape samples by features
%   avg_old = [1 n] old average of shape features
%   cov_old = [n n] old covariance of shape features by features
%   m_old   = [int] old sample counter
%
% OUTPUT
%   avg_new = [1 n] new average of shape features
%   cov_new = [n n] new covariance of shape features by features
%   m_new   = [int] new sample counter

m_obs = size(obs, 1);
avg_obs = mean(obs, 1);

% Sample count
m_new = m_obs + m_old;

% First data
if m_old <= 0

    % Average
    avg_new = avg_obs;

    % Zero mean
    x1 = bsxfun(@minus, obs, avg_obs);

    % Covariance
    cov_obs = x1' * x1;
    cov_new = cov_obs / (m_new - 1);

% Update
else

    % Average
    avg_new = avg_old + (avg_obs - avg_old) * (m_obs / m_new);

    % Zero mean
    x1 = bsxfun(@minus, obs, avg_old);
    x2 = bsxfun(@minus, obs, avg_new);

    % Covariance
    cov_obs = x1' * x2;
    cov_new = cov_obs / (m_new - 1) + cov_old * ((m_old - 1) / (m_new - 1));

end

function test_case()

m = 1000;
n = 54;
X = double(rand(m, n));

% Incremental
avg_inc = [];
cov_inc = [];
m_inc = 0;
for i = 1:10
    obs = X(1 + (i-1) * 100:i * 100, :);
    [avg_inc, cov_inc, m_inc] = jt_covariance_incremental(obs, avg_inc, cov_inc, m_inc);
end

% Non-incremental
X = bsxfun(@minus, X, mean(X, 1));
cov_noninc = cov(X);

disp(max(abs(cov_inc(:) - cov_noninc(:))));

disp(m_inc-m);
