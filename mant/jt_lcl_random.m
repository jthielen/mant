function [layout, value] = jt_lcl_random(x)
% [layout, xcorr] = jt_lcl_random(x)
% Random layout.
%
% INPUT
%   x = [m n] n variables of m samples
%
% OUTPUT
%   layout = [n n] optimal layout
%   value  = [flt] max pairwise correlation in layout

n = sqrt(size(x, 2));
if rem(n, 1) ~= 0; error('Input x is not sufficient for a square matrix: [%d %d].', size(x)); end

% Compute cross correlations
correlations = jt_correlation(x, x);

% Random layout
layout = randperm(n^2)';
neighbours = jt_findneighbours(layout);
value = jt_findworstneighbours(neighbours, correlations);

% Reshape layout
layout = reshape(layout, [1 n^2]);
