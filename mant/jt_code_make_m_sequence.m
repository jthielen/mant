function [code] = jt_code_make_m_sequence(m, v)
% [code] = jt_code_make_m_sequence(m, v)
% Generates a maximum length sequence.
% 
% INPUT
%   m = [int] register length (6)
%   v = [1 p] array of p feedback tab points ([6 1])
% 
% OUTPUT
%   code = [2^n-1 1] m-sequence of shape bits by code

% Check input
if nargin < 1 || isempty(m); m = 6; end
if nargin < 2 || isempty(v); v = [6 1]; end
    
% Create initial register
register = ones(1, m);

% Generate m-sequence
code = zeros(2^m - 1, 1);
for i = 1:2^m - 1
    code(i) = mod(sum(register(v)), 2);
    register = [code(i) register(1:end-1)];
end
