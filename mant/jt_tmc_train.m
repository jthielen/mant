function [classifier] = jt_tmc_train(data, cfg)
%[classifier] = jt_tmc_train(data, cfg)
% Trains a template matching classifier
%
% INPUT
%   data = [struct] data structure:
%       .X   = [c m k] training data of c channels, m samples, and k trials
%       .y   = [k 1]   training labels or targets of k trials, i.e., indexes in V
%       .V   = [s p]   one period of training sequences of s samples and p codes, should be at the same sample frequency as .X
%       .U   = [s q]   one period of testing sequences of s samples and q codes, should be at the same sample frequency as .X (default: V)
%   cfg = [struct] configuration structure
%         [struct] classifier with cfg and empty fields at retrainable properties
%
%   General:
%       .verbosity = [int] verbosity level: 0=off, 1=classifier, 2=classifier with cross-validated accuracy (1)
%       .n_folds   = [int] number of folds to use in the cross-validation if verbosity==3 (5)
%       .user      = [str] name of current user, only used if verbosity > 0 to view the classifier ('user')
%       .capfile   = [str] file name of electrode positions, only used if verbosity > 0 to view the spatial filter ('nt_cap64.loc')
%
%   Timing:
%       .fs               = [int] data sample frequency in hertz (256)
%       .segment_time     = [flt] data segment length in seconds (.1)
%       .min_time         = [flt] minimum trial length in seconds (.segmenttime)
%       .max_time         = [flt] maximum trial length in seconds (4.0)
%       .inter_trial_time = [flt] inter-trial time in seconds, only used for calculating the ITR if verbosity > 1 (0)
%       .delay            = [1 e] overall positive delay of onsets of events in seconds, e.g., marker transmission delay (0)
%       .latency_V        = [1 p] latencies for individual training sequences in seconds, e.g. rasterization delay (zeros(p, 1))
%       .latency_U        = [1 q] latencies for individual testing sequences in seconds, e.g. rasterization delay (zeros(q, 1))
%
%   Classification:
%       .method       = [str]  classification method: fix=fixed-length trials, margin=margin stopping, beta=beta stopping, bes0/bes1/bes2=Bayes stopping ('fix')
%       .metric       = [str]  scoring metric: correlation|inner ('correlation')
%       .zerotraining = [bool] whether or not zerotraining (false)
%       .accuracy     = [flt] targeted stopping accuracy (.95)
%       .cr           = [flt] cost ratio (1.0)
%       .pf           = [flt] targeted probability of error (0.05)
%       .pd           = [flt] targeted probabiity of detection (0.80)
%
%   Reconvolution
%       .L           = [1 e]  length of transient responses in seconds (0.3)
%       .event       = [str]  event type for decomposition: see jt_event_matrix ('duration')
%       .component   = [int]  CCA component to use (1)
%       .lx          = [int]  regularization for Wx (i.e., spatial filter)) between 0 (unreliable) and 1 (reliable) (1)
%                    = [1 c]  penalties for each channel c
%                    = [str]  penalty type for Wx (e.g., tukey)
%       .ly          = [int]  regularization for Wy (i.e., transient response(s)) between 0 (unreliable) and 1 (reliable) (1)
%                    = [1 l]  penalties for each response sample l
%                    = [str]  penalty type for Wy (e.g., tukey)
%       .lx_amp      = [flt]  amplifier for lx regularization penalties, i.e., maximum penalty (0.1)
%       .ly_amp      = [flt]  amplifier for ly regularization penalties, i.e., maximum penalty (0.01)
%       .ly_perc     = [flt]  relative parts of the taper that is regularized (.2)
%       .model_onset = [bool] whether or not to model the onset, uses L(end) as length (false)
%
%   Subset Selection:
%       .subset_V  = [1 p] training subset (1:p)
%       .subset_U  = [1 q] testing subset (1:q)
%                  = [str] optimization method, e.g. clustering
%       .n_classes = [int] number of training codes, only for optimization ([])
%
%   Layout Selection
%       .layout_V   = [1 p] training layout (1:p)
%       .layout_U   = [1 q] testing layout (1:q)
%                   = [str] optimization method, e.g. incremental
%       .neighbours = [x 2] neighbour pairs, only for otptimization ([])
%
%   Zero-training
%       .min_time_first = [flt] minimum trial length for first trial in seconds (.maxtime)
%       .accuracy_first = [flt] targeted accuracy for first trial (.accuracy)
%
% OUTPUT
%   classifier = [struct] classifier structure:
%       .cfg = [struct] configuration
%       .stim = [struct] stimuli structure matrices
%           .Mvs = [l s p] structure matrices for V, period==1, subset and layout are applied
%           .Mvw = [l s p] structure matrices for V, period>1, subset and layout are applied
%           .Mus = [l s q] structure matrices for U, period==1, subset and layout are applied
%           .Muw = [l s q] structure matrices for U, period>1, subset and layout are applied
%       .transients = [l 1] transient response(s)
%       .filter = [c 1] spatial filter
%       .model = [struct] covariance model
%           .count = [int] counter of data in model
%           .avg   = [N-D] running average
%           .cov   = [N-D] running covariance
%           .X     = [c z] stacked data of c channels and q samples so far
%           .M     = [l z] stacked structure matrices of classified labels of e response samples and q samples so far
%       .templates = [struct] templates
%           .Tvs = [s p] templates for V, period==1, subset and layout are applied
%           .Tvw = [s p] templates for V, period>1, subset and layout are applied
%           .Tus = [s q] templates for U, period==1, subset and layout are applied
%           .Tuw = [s q] templates for U, period>1, subset and layout are applied
%       .subset = [struct] subset
%           .V = [1 p] training subset for V
%           .U = [1 q] testing subset for U
%       .layout = [struct] layout
%           .V = [1 p] training layout for V
%           .U = [1 q] testing layout for U
%       .margins = [1 z] threshold margins for margin stopping model
%       .accuracy = [struct] accuracy estimation
%           .p   = [flt] estimate of the accuracy
%           .t   = [flt] estimate of the trial duration, i.e., forward stop
%           .itr = [flt] estimate of the Wolpaw information transfer rate
%       .view = [hdl] figure handle to classifier figure

if nargin<2 || isempty(cfg); cfg=[]; end
if jt_exists_in(data, 'y'); data.y = data.y(:); end
if ~jt_exists_in(data, 'U'); data.U = data.V; end
data.X = double(data.X);

% Variables
[n_channels, n_samples, n_trials] = size(data.X);
[n_samples_codes, n_classes_V] = size(data.V);
n_classes_U = size(data.U, 2);

%--------------------------------------------------------------------------
% Initialization

if isfield(cfg,'cfg') 
    % The cfg is an 'old' classifier to be re-used
    classifier = cfg;
    cfg = classifier.cfg;
else
    % Initialize a new classifier with specified cfg
    classifier = struct();
    classifier.cfg = struct();
end

%--------------------------------------------------------------------------
% Defaults

% General
classifier.cfg.verbosity = jt_parse_cfg(cfg, 'verbosity', 1);
classifier.cfg.n_folds = jt_parse_cfg(cfg, 'n_folds', 5);
classifier.cfg.user = jt_parse_cfg(cfg, 'user', 'user');
classifier.cfg.capfile = jt_parse_cfg(cfg, 'capfile', 'nt_cap64.loc');

% Timing
classifier.cfg.fs = jt_parse_cfg(cfg, 'fs', 256);
classifier.cfg.segment_time = jt_parse_cfg(cfg, 'segment_time', 0.1);
classifier.cfg.min_time = jt_parse_cfg(cfg, 'min_time', classifier.cfg.segment_time);
classifier.cfg.max_time = jt_parse_cfg(cfg, 'max_time', 4.0);
classifier.cfg.inter_trial_time = jt_parse_cfg(cfg, 'inter_trial_time', 0);
classifier.cfg.delay = jt_parse_cfg(cfg, 'delay', 0);
classifier.cfg.latency_V = jt_parse_cfg(cfg, 'latency_V', zeros(n_classes_V, 1));
classifier.cfg.latency_U = jt_parse_cfg(cfg, 'latency_U', zeros(n_classes_U, 1));

% Classification
classifier.cfg.method = jt_parse_cfg(cfg, 'method', 'fix');
classifier.cfg.metric = jt_parse_cfg(cfg, 'metric', 'correlation');
classifier.cfg.zerotraining = jt_parse_cfg(cfg, 'zerotraining', false);
classifier.cfg.accuracy = jt_parse_cfg(cfg, 'accuracy', 0.95);
classifier.cfg.cr = jt_parse_cfg(cfg, 'cr', 1.0);
classifier.cfg.pf = jt_parse_cfg(cfg, 'pf', 0.05);
classifier.cfg.pd = jt_parse_cfg(cfg, 'pd', 0.80);
classifier.cfg.patience = jt_parse_cfg(cfg, 'patience', 3);

% Reconvolution
classifier.cfg.L = jt_parse_cfg(cfg, 'L', 0.3);
classifier.cfg.event = jt_parse_cfg(cfg, 'event', 'duration');
classifier.cfg.component = jt_parse_cfg(cfg, 'component', 1);
classifier.cfg.lx = jt_parse_cfg(cfg, 'lx', 1);
classifier.cfg.ly = jt_parse_cfg(cfg, 'ly', 1);
classifier.cfg.lx_amp = jt_parse_cfg(cfg, 'lx_amp', 0.1);
classifier.cfg.ly_amp = jt_parse_cfg(cfg, 'ly_amp', 0.01);
classifier.cfg.ly_perc = jt_parse_cfg(cfg, 'ly_perc', 0.2);
classifier.cfg.model_onset = jt_parse_cfg(cfg, 'model_onset', false);

% Subset
classifier.cfg.subset_V = jt_parse_cfg(cfg, 'subset_V', 1:n_classes_V);
classifier.cfg.subset_U = jt_parse_cfg(cfg, 'subset_U', 1:n_classes_U);
classifier.cfg.n_classes = jt_parse_cfg(cfg, 'n_classes', []);

% Layout
classifier.cfg.layout_V = jt_parse_cfg(cfg, 'layout_V', 1:n_classes_V);
classifier.cfg.layout_U = jt_parse_cfg(cfg, 'layout_U', 1:n_classes_U);
classifier.cfg.neighbours = jt_parse_cfg(cfg, 'neighbours', []);

% Zerotraining
classifier.cfg.accuracy_first = jt_parse_cfg(cfg, 'accuracy_first', classifier.cfg.accuracy);
classifier.cfg.min_time_first = jt_parse_cfg(cfg, 'min_time_first', classifier.cfg.segment_time);

% Classification should at least use one segment!
classifier.cfg.min_time = max(classifier.cfg.min_time, classifier.cfg.segment_time);
classifier.cfg.min_time_first = max(classifier.cfg.min_time_first, classifier.cfg.segment_time);

%--------------------------------------------------------------------------
% Structure matrices

if ~jt_exists_in(classifier, 'stim') || ~jt_exists_in(classifier.stim, {'Mvs','Mvw','Mus','Muw'})
    
    % Build structure matrices
    M = jt_reconvolution_structure_matrix(repmat(cat(2, data.V, data.U), [2 1]), struct(...
        'L', floor(classifier.cfg.L * classifier.cfg.fs),...
        'delay', floor(classifier.cfg.delay * classifier.cfg.fs),...
        'event', classifier.cfg.event,...
        'modelonset', classifier.cfg.model_onset));
    classifier.stim.Mvs = M(:, 1:n_samples_codes, 1:n_classes_V);
    classifier.stim.Mvw = M(:, 1+n_samples_codes:end, 1:n_classes_V);
    classifier.stim.Mus = M(:, 1:n_samples_codes, n_classes_V+1:end);
    classifier.stim.Muw = M(:, 1+n_samples_codes:end, n_classes_V+1:end);

    % Make sure there is an L for each event
    if numel(classifier.cfg.L) == 1
        classifier.cfg.L = repmat(classifier.cfg.L, [1 floor(size(M, 1) / (classifier.cfg.L * classifier.cfg.fs))]); 
    end
    
end

%--------------------------------------------------------------------------
% Training subset and layout

classifier.subset.V = classifier.cfg.subset_V;
classifier.layout.V = classifier.cfg.layout_V;
classifier.stim.Mvs = classifier.stim.Mvs(:, :, classifier.subset.V(classifier.layout.V));
classifier.stim.Mvw = classifier.stim.Mvw(:, :, classifier.subset.V(classifier.layout.V));

%--------------------------------------------------------------------------
% Latencies

if ~all(classifier.cfg.latency_V == 0)
    assert(n_classes_V == numel(classifier.cfg.latency_V));
    for i = 1:n_classes_V
        shift = floor(classifier.cfg.latency_V(i) * classifier.cfg.fs);
        classifier.stim.Mvs(:, :, i) = circshift(classifier.stim.Mvs(:, :, i), shift, 2);
        classifier.stim.Mvw(:, :, i) = circshift(classifier.stim.Mvw(:, :, i), shift, 2);
        classifier.stim.Mvs(:, 1:shift, i) = 0;
    end
end

if ~all(classifier.cfg.latency_U == 0)
    assert(n_classes_U == numel(classifier.cfg.latency_U));
    for i = 1:n_classes_U
        shift = floor(classifier.cfg.latency_U(i) * classifier.cfg.fs);
        classifier.stim.Mus(:, :, i) = circshift(classifier.stim.Mus(:, :, i), shift, 2);
        classifier.stim.Muw(:, :, i) = circshift(classifier.stim.Muw(:, :, i), shift, 2);
        classifier.stim.Mus(:, 1:shift, i) = 0;
    end
end

%--------------------------------------------------------------------------
% Deconvolution

if ~jt_exists_in(classifier, {'transients', 'filter'})
    
    % Initialize
    classifier.filter = [];
    classifier.transients = [];
    classifier.model = [];

    % Decompose
    if ~isempty(data.X) && ~isempty(data.y)
        M = cat(2, classifier.stim.Mvs, repmat(classifier.stim.Mvw, [1 ceil(n_samples/n_samples_codes)-1 1]));
        M = M(:, 1:n_samples, :);
        [classifier.filter, classifier.transients, classifier.model] = jt_reconvolution_decompose(data.X, data.y, M, cfg, classifier.model);
    end
    
end

%--------------------------------------------------------------------------
% Convolution

if ~jt_exists_in(classifier, 'templates') || ~jt_exists_in(classifier.templates, {'Tvs','Tvw','Tus','Tuw'})
    
    % Initialize
    classifier.templates.Tvs = [];
    classifier.templates.Tvw = [];
    classifier.templates.Tus = [];
    classifier.templates.Tuw = [];

    % Convolution
    if ~isempty(classifier.transients)
        classifier.templates.Tvs = jt_reconvolution_compose(classifier.stim.Mvs, classifier.transients);
        classifier.templates.Tvw = jt_reconvolution_compose(classifier.stim.Mvw, classifier.transients);
        classifier.templates.Tus = jt_reconvolution_compose(classifier.stim.Mus, classifier.transients);
        classifier.templates.Tuw = jt_reconvolution_compose(classifier.stim.Muw, classifier.transients);
    end
    
end

%--------------------------------------------------------------------------
% Testing subset

if ~jt_exists_in(classifier.subset, 'U')
    
    % Select subset
    if isnumeric(classifier.cfg.subset_U)
        classifier.subset.U = classifier.cfg.subset_U;
    else
        switch classifier.cfg.subsetU

            case 'no'
                classifier.subset.U = 1:n_classes_U;

            case 'default_36'
                in = load('nt_subset.mat');
                classifier.subset.U = in.subset;
                n_classes_U = numel(classifier.subset.U);

            case {'yes', 'clustering'}
                if classifier.cfg.zerotraining 
                    error('Impossible to optimize a subset in zerotraining mode.'); 
                end
                if isempty(classifier.cfg.nclasses)
                    error('Subset optimization requires a specification of the number of classes.')
                end
                templates = cat(1, classifier.templates.Tus, classifier.templates.Tuw);
                classifier.subset.U = jt_lcs_clustering(templates, classifier.cfg.n_classes, classifier.cfg.segment_time * classifier.cfg.fs);
                
                n_classes_U = numel(classifier.subset.U);
                classifier.model.n = n_classes_U;

            otherwise
                error('Unknown subset method %s.',classifier.cfg.subset_U);
        end
    end

    % Apply subset
    classifier.stim.Mus = classifier.stim.Mus(:, :, classifier.subset.U);
    classifier.stim.Muw = classifier.stim.Muw(:, :, classifier.subset.U);
    if ~isempty(classifier.templates.Tus) && ~isempty(classifier.templates.Tuw)
        classifier.templates.Tus = classifier.templates.Tus(:, classifier.subset.U);
        classifier.templates.Tuw = classifier.templates.Tuw(:, classifier.subset.U);
    end
    
end

%--------------------------------------------------------------------------
% Testing layout

if ~jt_exists_in(classifier.layout, 'U')
    
    % Select layout
    if isnumeric(classifier.cfg.layout_U)
        classifier.layout.U = classifier.cfg.layout_U;
    else
        switch classifier.cfg.layoutU

            case 'no'
                classifier.layout.U = 1:n_classes_U;

            case 'default_36'
                in = load('nt_layout.mat');
                classifier.layout.U = in.layout;

            case {'yes', 'incremental'}
                if classifier.cfg.zerotraining
                    error('Impossible to train a layout in zerotraining mode.'); 
                end
                templates = cat(1, classifier.templates.Tus, classifier.templates.Tuw);
                if isnumeric(classifier.cfg.neighbours) && numel(classifier.cfg.neighbours) == 2
                    neighbours = jt_findneighbours(reshape((1:n_classes_U)', classifier.cfg.neighbours));
                end
                classifier.layout.U = jt_lcl_incremental(templates, neighbours, classifier.cfg.segment_time * classifier.cfg.fs);

            otherwise
                error('Unknown layout method %s.', classifier.cfg.layout_U);
        end
    end

    % Apply layout
    classifier.stim.Mus = classifier.stim.Mus(:, :, classifier.layout.U);
    classifier.stim.Muw = classifier.stim.Muw(:, :, classifier.layout.U);
    if ~isempty(classifier.templates.Tus) && ~isempty(classifier.templates.Tuw)
        classifier.templates.Tus = classifier.templates.Tus(:, classifier.layout.U);
        classifier.templates.Tuw = classifier.templates.Tuw(:, classifier.layout.U);
    end
    
end

%--------------------------------------------------------------------------
% Stopping

if ~jt_exists_in(classifier, 'stopping')
    
    % Initialize
    classifier.stopping = struct();

    % Margins
    switch classifier.cfg.method

        case {'fix', 'beta'}
            % Do not require any calibration

        case {'static_max_itr', 'static_max_acc', 'static_tgt_acc'}
            if isempty(classifier.templates.Tvs) || isempty(classifier.templates.Tvw) || isempty(data.X) || isempty(data.y)
                error('Static stopping requires data and templates.');
            end
            classifier.stopping.time = jt_stopping_static(data, classifier.cfg); 

        case 'margin'
            if isempty(classifier.templates.Tvs) || isempty(classifier.templates.Tvw) || isempty(data.X) || isempty(data.y)
                error('Dynamic stopping with margin requires data and templates.');
            end
            fX = reshape(classifier.filter' * reshape(data.X, [n_channels n_samples * n_trials]), [n_samples n_trials]);
            Tu = cat(1, classifier.templates.Tvs, repmat(classifier.templates.Tvw, [ceil(n_samples / n_samples_codes) 1]));
            Tu = Tu(1:n_samples, :);
            classifier.stopping.margins = jt_stopping_margins(fX, data.y, Tu, struct(...
                'segmentlength', classifier.cfg.segment_time * classifier.cfg.fs,...
                'accuracy', classifier.cfg.accuracy ^ (1 / floor(classifier.cfg.max_time / classifier.cfg.segment_time)), ...
                'metric', classifier.cfg.metric)); 

        case {'bes0', 'bes1', 'bes2'}
            if isempty(classifier.templates.Tvs) || isempty(classifier.templates.Tvw) || isempty(data.X) || isempty(data.y)
                error('Dynamic stopping with bayes requires data and templates.');
            end
            fX = reshape(classifier.filter' * reshape(data.X, [n_channels n_samples * n_trials]), [n_samples n_trials]);
            Tu = cat(1, classifier.templates.Tvs, repmat(classifier.templates.Tvw, [ceil(n_samples / n_samples_codes) 1]));
            Tu = Tu(1:n_samples, :);
            Tu = Tu - mean(Tu, 1);
            [classifier.stopping.eta, classifier.stopping.alpha, classifier.stopping.sigma, ...
                classifier.stopping.b0, classifier.stopping.b1, classifier.stopping.s0, classifier.stopping.s1, ...
                classifier.stopping.pf, classifier.stopping.pm] = jt_stopping_bayes(fX, data.y, Tu, struct(...
                'segmentlength', classifier.cfg.segment_time * classifier.cfg.fs,...
                'cr', classifier.cfg.cr));

        otherwise
            error('Unknown stopping method: %s', classifier.cfg.method);
    end
    
end

%--------------------------------------------------------------------------
% Accuracy

if ~jt_exists_in(classifier, 'accuracy')
    
    % Initialize
    classifier.accuracy = [];
    classifier.accuracy.accuracy = [];
    classifier.accuracy.time = [];
    classifier.accuracy.itr = [];

    % Check if accuracy can be computed
    if classifier.cfg.verbosity > 1 && (~jt_exists_in(classifier.templates, {'Tvs','Tvw'}) || ~jt_exists_in(data, {'X','y'}) )
        error('For accuracy estimation data is required.');
    end

    % Accuracy
    if classifier.cfg.verbosity > 1

        % Cross-validation
        tmp = classifier.cfg;
        tmp.zerotraining = false;
        results = jt_tmc_cv(data, tmp, classifier.cfg.n_folds);
        classifier.accuracy.accuracy = mean(results.accuracy);
        classifier.accuracy.time = mean(results.time);
        classifier.accuracy.itr = jt_itr(n_classes_V, classifier.accuracy.accuracy, classifier.accuracy.time + classifier.cfg.inter_trial_time);

    end
    
end

%--------------------------------------------------------------------------
% View

if classifier.cfg.verbosity > 0
    classifier = jt_tmc_view(classifier);
end
