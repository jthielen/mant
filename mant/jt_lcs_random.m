function [subset, value] = jt_lcs_random(x, k)
% [subset, value] = jt_lcs_random(x, k)
% Finds the least correlating subset in an random manner.
%
% INPUT
%   x = [m n] m samples of n variables
%   k = [int] number of variables in subset
%
% OUTPUT
%   subset = [1 k] positions of the subset in x
%   value  = [flt] correlation value of the subset

num = size(x, 2);
if k > num; error('x does not hold n variables!'); end

% Compute correlations
rho = jt_correlation(x, x);
rho = max(rho, [], 3);
rho(logical(eye(num))) = NaN; %ignore diagonal

% Select subset
index = randperm(num);

% Compute output
subset = index(1:k);
value = nanmax(nanmax(rho(subset, subset)));
