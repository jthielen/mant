function [X, ret] = jt_preproc_basic(X, cfg)
% [X, ret] = jt_preproc_basic(X, cfg)
% Preprocess data by performing: linear detrend, re-referencing, 
% spectral filter and removal of baseline data.
%
% INPUT
%   X   = [c t n]  data matrix of channels by time by trials
%   cfg = [struct] configuration structure:
%       .verbosity = [int]  verbosity level (0)
%       .fs        = [int]  sample frequency (256)
%       .chn_thres = [bool] threshold for channel outlier removal (false)
%       .trl_thres = [bool] threshold for trial outlier removal (false)
%       .reref     = [str]  re-reference procedure ('no')
%       .bands     = {n 2}  cell of pass bands ({[5 48],[52 120]})
%       .fronttime = [flt]  time in front of data to remove (0)
%
% OUTPUT
%   X   = [c t n]  preprocessed data matrix of channels by time by trials
%   ret = [struct] summary of pre-processing
%       .rmvchn = [1 c] 1 if channel outlier, 0 otherwise.
%       .rmvtrl = [1 n] 1 if trial outlier, 0 otherwise.

if isempty(X); ret = []; return; end
if nargin < 2 || isempty(cfg); cfg = []; end
verbosity = jt_parse_cfg(cfg, 'verbosity', 0);
fs = jt_parse_cfg(cfg, 'fs', 256);
chn_thres = jt_parse_cfg(cfg, 'chnthres', false);
trl_thres = jt_parse_cfg(cfg, 'trlthres', false);
reref = jt_parse_cfg(cfg, 'reref', 'no');
bands = jt_parse_cfg(cfg, 'bands', {{[5 48], [52 120]}});
fronttime = jt_parse_cfg(cfg, 'fronttime', 0);
[n_channels, n_samples, n_trials] = size(X);

% Remove outliers in channels 
if chn_thres && n_channels > 1
    ret.rmvchn = idOutliers(X, 1, 3);
    ret.rmvchn = squeeze(ret.rmvchn);
    X(ret.rmvchn, :, :) = [];
    if verbosity > 0
        fprintf('Removed %d channels.\n', sum(ret.rmvchn)); 
    end
else
    ret.rmvchn = false(1, n_channels);
end

% Remove outliers in trials 
if trl_thres && n_trials > 1
    ret.rmvtrl = idOutliers(X, 3, 3);
    ret.rmvtrl = squeeze(ret.rmvtrl);
    X(:,:,ret.rmvtrl) = [];
    if verbosity > 0
        fprintf('Removed %d trials.\n', sum(ret.rmvtrl)); end
else
    ret.rmvtrl = false(1, n_trials);
end

% Linear detrend
X = detrend(X, 2, 1);

% Rereference
switch lower(reref)
    case 'car'
        X = repop(X, '-', mean(X));
    case 'no'
    otherwise
        error('Unknown reref method: %s',reref)
end

% Spectral filter
if ~strcmp(bands, 'no')
    X = fftfilter(X, ...
        mkFilter(floor(n_samples / 2), bands, fs / n_samples), ...
        n_samples, 2, 1, [], 0, [], -1);
end

% Remove baseline data
if fronttime > 0
    X = X(:, fronttime * fs + 1:end, :);
    if verbosity>0
        fprintf('Removed %d seconds of baseline.\n', fronttime); 
    end
end
