function [itr] = jt_itr(N, P, T, method)
% [itr] = jt_itr(N, P, T, method)
% Computes the information transfer rate.
%
% INPUT
%   N      = [int] number of classes
%   P      = [flt] classification accuracy
%   T      = [flt] duration of one classification in seconds
%   method = [str] method to compute itr ('ritr')
%
% OUTPUT
%   B = [flt] information transfer rate

if nargin < 4 || isempty(method); method = 'ritr'; end

% Get rid of ones and zeros
P(P >= 1) = 1 - eps;
P(P <= 0) = eps;

% Compute bits/trial
switch lower(method)
    
    case 'itr'  
        % bits/trial
        itr = wolpaw_itr(N, P); 
        
    case 'ritr' 
        % bits/minute
        itr = (60 ./ T) .* wolpaw_itr(N, P); 
        
    case 'cr'
        % char/trial
        itr = wolpaw_itr(N, P) ./ log2(N); 
        
    case 'cpm'
        % char/min
        itr = (60 ./ T) .* wolpaw_itr(N, P) ./ log2(N); 
        
    case 'spm'
        % char/min realistic
        itr = (60 ./ T) .* max(0, (P - (1 - P))); 
        
    case 'spm*'
        % bits/min including N
        itr = (60 ./ T) .* max(0, (P - (1 - P))) .* log2(N); 
         
    otherwise
        error('Unknown method: %s', method)
end

%--------------------------------------------------------------------------
function B = wolpaw_itr(N, P)
    B = log2(N) + P .* log2(P) + (1 - P) .* log2((1 - P) ./ (N - 1));
    