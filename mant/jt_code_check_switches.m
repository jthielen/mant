function [switches] = jt_code_check_switches(codes)
% [switches] = jt_code_check_switches(codes)
% Determines the number of on and off switches in a variable.
% 
% INPUT
%   codes = [m n] codes of shape bits by codes
%
% OUTPUT
%   switches = [2 n] amound of both on and off switches for all codes

% Shift var
scodes = circshift(codes, [1 0]);

% Last bit became first bit, redo
scodes(1, :) = 0;

% Find switches
switches = [ sum( codes & ~scodes,1); ... % On:  01
             sum(~codes &  scodes,1)];    % Off: 10
