function [codes] = jt_code_make_mgold_code(m, v, w, addmls)
% [codes] = jt_make_mgold_code(m, v, w, addmls)
% Generates modulated gold codes. 
%
% INPUT
%   m      = [int] register length (6)
%   v      = [1 p] array of p feedback tab points ([6 1])
%   w      = [1 q] array of q feedback tab points ([6 5 2 1])
%   addmls = [bool] wheter or not to add the m-sequences at the end (true)
% 
% OUTPUT
%   codes = [2*2^m-1 2^m+1] modulated Gold codes of shape bits by codes
%         = [2*2^m-1 2^m-1] Gold codes of shape bits by codes (w/o m-sequences)

% Check input
if nargin < 1 || isempty(m); m = 6; end
if nargin < 2 || isempty(v); v = [6 1]; end
if nargin < 3 || isempty(w); w = [6 5 2 1]; end
if nargin < 4 || isempty(addmls); addmls = true; end

% Generate gold codes
codes = jt_code_make_gold_code(m, v, w, addmls);

% Modulate gold codes
codes = jt_code_modulate(codes);
