function [margins] = jt_stopping_margins(X, y, T, cfg)
% [margins] = jt_stopping_margins(X, y, T, cfg)
% Learn stopping margins for dynamic stopping.
%
% INPUT
%   X = [m k] data of samples by trials
%   y = [k 1] labels for each trial
%   T = [m n] templates of samples by classes
%   cfg = [struct] configuration structure
%       .model          = [str]  model for learning margins ('normal')
%       .segmentlength  = [int]  segment length samples (100)
%       .alpha          = [flt]  probability of Type I error, 1-alpha is the confidence level (.05)
%       .accuracy       = [flt]  targeted classification rate (.99)
%       .marginmin      = [flt]  minimum of margins (0.01)
%       .marginmax      = [flt]  maximum of margins (0.99)
%       .marginstep     = [flt]  stepsize of margins (0.05)
%       .smooth         = [bool] whether or not to smoot margins (true)
%
% OUTPUT
%   margins = [1 n] margin tresholds of segment (rows) by segment-length (columns)

% Defaults
if size(T, 2) < 2; error('Stopping cannot be trained on one-class data.'); end
if size(T, 1) ~= size(X, 1); error('X and T have different number of samples'); end
if nargin < 4 || isempty(cfg); cfg = []; end
model = jt_parse_cfg(cfg, 'model', 'normal');  % model to use
metric = jt_parse_cfg(cfg, 'metric', 'correlation');  % metric to use
segmentlength = jt_parse_cfg(cfg, 'segmentlength', 100);  % length of segment in samples
alpha = jt_parse_cfg(cfg, 'alpha', .05);  % probability of Type I error, 1-alpha is the confidence level
accuracy = jt_parse_cfg(cfg, 'accuracy', .99);  % targeted accuracy
smooth = jt_parse_cfg(cfg, 'smooth', true);  % whether or not to smoot margins
if strcmpi(metric, 'correlation')
    marginmin = jt_parse_cfg(cfg, 'marginmin', 0.01);  % min value for margin
    marginmax = jt_parse_cfg(cfg, 'marginmax', 0.99);  % max value for margin
    marginstep = jt_parse_cfg(cfg, 'marginstep', 0.05);  % value to increase margin with
else
    marginmin = jt_parse_cfg(cfg, 'marginmin', 0);  % min value for margin
    marginmax = jt_parse_cfg(cfg, 'marginmax', 250);  % max value for margin
    marginstep = jt_parse_cfg(cfg, 'marginstep', 10);  % value to increase margin with
end

% Variables
n_samples = size(X, 1);
n_segments = floor(n_samples / segmentlength);
margins = nan(n_segments, 1);

% For all segments, find correlation differences and correctness
switch metric
    case 'correlation'
        similarity = jt_correlation(T, X, struct('method', 'fwd', 'lenseg', segmentlength, 'numseg', n_segments));
    case 'inner'
        similarity = zeros(size(T, 2), size(X, 2), n_segments);
        for i_segment = 1:n_segments
            similarity(:, :, i_segment) = (T(1:i_segment * segmentlength, :) - mean(T(1:i_segment * segmentlength, :), 1))' * X(1:i_segment * segmentlength, :);
        end
    otherwise
        error('Unknown metric: %s', metric)
end
[val, idx] = sort(similarity, 1, 'descend');
D = squeeze(val(1, :, :) - val(2, :, :));
C = squeeze(idx(1, :, :)) == repmat(y(:), [1 n_segments]);
        
% Initialize variables
marginaxis = marginmin:marginstep:marginmax;
if strcmpi(model, 'binom')
    z = norminv(1 - alpha);
end

% Over segment length
for i = 1:n_segments 
    
    % Take all segments with equal length
    d = D(:, i); 
    d = d(:);
    c = C(:, i); 
    c = c(:);
    
    % Remove nans (upper diagonal)
    nanidx = isnan(d);
    d(nanidx) = [];
    c(nanidx) = [];

    % Compute histograms
    wronghist = hist(d(logical(~c)), marginaxis); 
    righthist = hist(d(logical( c)), marginaxis); 

    % Learn margin
    switch model

        case 'normal'
            % Reverse cummulative: how many will stop when margin > x
            wronghist = cumsum(wronghist(end:-1:1));
            wronghist = wronghist(end:-1:1);
            righthist = cumsum(righthist(end:-1:1));
            righthist = righthist(end:-1:1);

            % Compute performance
            perfs = righthist ./ (wronghist + righthist);

            % Find margin that makes global performance above targeted accuracy
            tresholdi = find(perfs >= accuracy, 1);

        case 'binom'
            % Cummulative
            wronghist = cumsum(wronghist);
            righthist = cumsum(righthist);
            N = righthist + wronghist;

            % Compute proportions
            P = righthist ./ N;

            % Compute bounds of the binomial confidence interval (Wilson score interval)
            % cilb = max(0, (2 .* N .* P + z^2 - (z .* sqrt(z^2 - 1 ./ N + 4 * N .* P .* (1 - P) + (4 * P - 2)) + 1) ) ./ (2 * (N + z^2)));
            ciub = max(0, (2 .* N .* P + z^2 + (z .* sqrt(z^2 - 1 ./ N + 4 * N .* P .* (1 - P) + (4 * P - 2)) + 1) ) ./ (2 * (N + z^2)));

            % Find margin that makes global performance above targeted accuracy
            tresholdi = find(ciub >= accuracy, 1);

        otherwise
            error('Unknown model: %d.', model);
    end

    % Update thresholds and stopped trials
    if isempty(tresholdi)  % No stoppers
        margins(i) = marginmax;
    else  % Stoppers
        margins(i) = marginaxis(tresholdi);
    end
end

% Smooth margins
if smooth
    mrg = 0;
    for i = n_segments:-1:1
        if margins(i) <= mrg
            margins(i) = mrg;
        else
            mrg = margins(i);
        end
    end
end
