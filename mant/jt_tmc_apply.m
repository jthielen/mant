function [labels, results, classifier] = jt_tmc_apply(classifier, X)
%[labels, results, classifier] = jt_tmc_apply(classifier, X, active_codes)
%Applies the classifier to single-trial or multi-trial data.
%
% INPUT
%   classifier   = [struct] classifier structure
%   X            = [c m k]  data of channels by samples by trials
%
% OUTPUT  
%   labels  = [k 1]    the predicted labels, NaN if below threshold,
%                      negative label if below threshold and not forced
%                      stop
%   results = [struct] addition results of the classifier
%       .score  = [k 1] max score corresponding to classified label for k trials
%       .scores = [k n] score matrix of all n templates by k trials
%       .time   = [k 1] trial duration (i.e., forward step) in seconds of k trials
%   classifier = [struct] updated classifier

% Make data double precision always
X = double(X);

% Classify
if size(X, 3) == 1
    [labels, results, classifier] = jt_tmc_apply_single_trial(classifier, X);
else
    [labels, results, classifier] = jt_tmc_apply_multi_trial(classifier, X);
end

%--------------------------------------------------------------------------
function [label, result, classifier] = jt_tmc_apply_single_trial(classifier, X)
    
    % Initialize return variables
    label = NaN;
    result = struct('score', NaN, 'scores', NaN, 'time', NaN);
    
    % Extract sizes and settings
    n_samples = size(X, 2);
    n_template_samples = size(classifier.stim.Mus, 2);
    n_classes = size(classifier.stim.Mus, 3);
    n_segment_samples = floor(classifier.cfg.segment_time * classifier.cfg.fs);
    n_segments = floor(n_samples / n_segment_samples);
    min_segments = floor(classifier.cfg.min_time / classifier.cfg.segment_time);
    max_segments = floor(classifier.cfg.max_time / classifier.cfg.segment_time);
    min_segments_first = floor(classifier.cfg.min_time_first / classifier.cfg.segment_time);

    % Skip classification if not yet a full datasegment is collected
    if n_segments == 0 || mod(n_samples / n_segment_samples, 1) > 0
        return
    end
    
    %----------------------------------------------------------------------
    % Zerotraining update models
    
    if classifier.cfg.zerotraining
        idx = 1 + (n_segments-1) * n_segment_samples:n_segments * n_segment_samples;
        M = cat(2,...
            classifier.stim.Mus(:, idx(idx <= n_template_samples), :),...
            classifier.stim.Muw(:, mod(idx(idx > n_template_samples) - 1, n_template_samples) + 1, :));
        [classifier.filter, classifier.transients, classifier.model] = jt_reconvolution_decompose(X(:, idx), [], M, classifier.cfg, classifier.model);
        classifier.templates.Tus = jt_reconvolution_compose(classifier.stim.Mus, classifier.transients);
        classifier.templates.Tuw = jt_reconvolution_compose(classifier.stim.Muw, classifier.transients);
    end
    
    if ~strcmpi(classifier.cfg.accuracy, 'itr')

        % Check if this is the first trial
        if classifier.cfg.zerotraining && classifier.model.count < classifier.cfg.min_time_first * classifier.cfg.fs
            first_trial = true;
            accuracy = classifier.cfg.accuracy_first;
        else
            first_trial = false;
            accuracy = classifier.cfg.accuracy;
        end
    
        % Correction for multiple comparisons
        accuracy = accuracy ^ (1 / max_segments);
    end
    
    %----------------------------------------------------------------------
    % Spatially filter data
    
    if classifier.cfg.zerotraining
        % Loop over classes (as they have individual filters)
        fX = zeros(n_samples, n_classes);
        for i = 1:n_classes
            fX(:, i) = X' * classifier.filter(:, i);
        end
    else
        % Simple product
        fX = X' * classifier.filter;
    end
    
    %----------------------------------------------------------------------
    % Compute scores
        
    % Prepare templates
    idx = 1:n_samples;
    T = cat(1, ...
        classifier.templates.Tus(idx(idx <= n_template_samples), :), ...
        classifier.templates.Tuw(mod(idx(idx > n_template_samples) - 1, n_template_samples) + 1, :));

    % Zero-mean
    T = T - mean(T, 1);

    % Compute score
    % N.B. score is [n_classes x 1] if supervised
    % N.B. score is [n_classes x n_classes] if zero-training
    switch classifier.cfg.metric
        case 'correlation'
            scores = jt_correlation(T, fX);
        case 'inner'
            scores = T' * fX;
        otherwise
            error('Unknown metric: %s', classifier.cfg.metric)
    end
    
    % Zerotraining: select auto-models
    % N.B. scores will be [n_classes x 1] like supervised
    if classifier.cfg.zerotraining
        scores = diag(scores);
    end

    %----------------------------------------------------------------------
    % Classification

    % Maximize scores over classes
    [score_max, score_max_idx] = max(scores);
    
    %----------------------------------------------------------------------
    % Dynamic stopping
    
    stop = false;
    
    switch classifier.cfg.method

        case 'fix'

            % Stop if trial length is reached
            if n_segments >= max_segments
                stop = true;
            end

        case {'static_max_itr', 'static_max_acc', 'static_tgt_acc'}

            % Stop if time reached
            if n_segments >= classifier.stopping.time / classifier.cfg.segment_time
                stop = true;
            end
        
        case 'margin'
            
            % Compute difference score
            scores_sorted = sort(scores, 1, 'descend');
            score_second = scores_sorted(2);
            score_diff = score_max - score_second;

            % Stop if difference is larger than margin
            if score_diff > classifier.stopping.margins(n_segments) || n_segments >= max_segments
                stop = true;
            end
    
        case 'beta'
    
            % Convert to a range of [0 1] for Beta distribution
            % N.B. assumes correlation scores
            scores_h = (scores + 1) / 2;

            % For numerical stability
            scores_h(scores_h > 1) = 1;

            % Compute max and non-max correlations
            scores_h_sorted = sort(scores_h, 1, 'descend');
            score_h_max = scores_h_sorted(1);
            scores_h_non_max = scores_h_sorted(2:end);

            try
                % Fit a beta distribution to the non-max correlations
                beta = betafit(scores_h_non_max, 0.05);

                % Compute probability of maximum correlation
                p_max = betacdf(score_h_max, beta(1), beta(2)) ^ n_classes;
            catch
                fprintf('Could not fit beta, setting p_max to 0.\n')
                p_max = 0;
            end

            % Stop if probability is larger than targeted accuracy
            if p_max > accuracy || n_segments >= max_segments
                stop = true;
            end

        case 'bes0'

            % Stop if the score is larger than the decision boundary (eta)
            if score_max > classifier.stopping.eta(n_segments) || n_segments >= max_segments
                stop = true;
            end

        case 'bes1'

            % Replace target pf/pd with min/max of learned pf/pm
            if min(classifier.stopping.pf) > classifier.cfg.pf
                classifier.cfg.pf = min(classifier.stopping.pf);
                fprintf('Changed target pf to %.3f\n', classifier.cfg.pf);
            end
            if min(classifier.stopping.pm) > (1 - classifier.cfg.pd)
                classifier.cfg.pd = 1 - min(classifier.stopping.pm);
                fprintf('Changed target pd to %.3f\n', classifier.cfg.pd);
            end

            % Stop if the score is larger than the decision boundary (eta)
            % and the predicted probability of error is much lower than the target
            if score_max > classifier.stopping.eta(n_segments) && ...
                classifier.stopping.pf(n_segments) < classifier.cfg.pf && ...
                classifier.stopping.pm(n_segments) < (1 - classifier.cfg.pd) || ...
                n_segments >= max_segments
                stop = true;
            end

        case 'bes2'

            % Replace target pf/pd with min/max of learned pf/pm
            if min(classifier.stopping.pf) > classifier.cfg.pf
                classifier.cfg.pf = min(classifier.stopping.pf);
                fprintf('Changed target pf to %.3f\n', classifier.cfg.pf);
            end
            if min(classifier.stopping.pm) > (1 - classifier.cfg.pd)
                classifier.cfg.pd = 1 - min(classifier.stopping.pm);
                fprintf('Changed target pd to %.3f\n', classifier.cfg.pd);
            end

            % Find intersection of pf and pm to find optimal eta
            idx_pf = find(classifier.stopping.pf <= classifier.cfg.pf);
            idx_pm = find(classifier.stopping.pm <= (1 - classifier.cfg.pd));
            idx = intersect(idx_pf, idx_pm);
            eta = classifier.stopping.eta(idx(1));
            
            % Stop if the score is larger than the decision boundary (eta)
            % and a minimum probability of detection is reached
            if score_max > eta || n_segments >= max_segments
                stop = true;
            end

        
        otherwise
        
            error('Unknown stopping method: %s', classifier.cfg.method);
        
    end

    % Prevent classification if not sufficient data recorded
    if n_segments < min_segments || classifier.cfg.zerotraining && first_trial && n_segments < min_segments_first
        stop = false;
    end
    
    %----------------------------------------------------------------------
    % Results

    % Set label
    if stop
        label = score_max_idx;
    end
    
    % Set result structure
    result.score = score_max;
    result.scores = scores;
    result.time = n_segments * classifier.cfg.segment_time;
    
    %----------------------------------------------------------------------
    % Zerotraining select best model if classified
    
    if classifier.cfg.zerotraining && label > 0
        classifier.filter = classifier.filter(:, label);
        classifier.transients = classifier.transients(:, label);
        classifier.model.avg = classifier.model.avg(:, label);
        classifier.model.cov = classifier.model.cov(:, :, label);
        classifier.templates.Tus = jt_reconvolution_compose(classifier.stim.Mus, classifier.transients);
        classifier.templates.Tuw = jt_reconvolution_compose(classifier.stim.Muw, classifier.transients);
    end

%--------------------------------------------------------------------------
function [labels ,results, classifier] = jt_tmc_apply_multi_trial(classifier, X)
    
    % Variables
    [~, n_samples, n_trials] = size(X);
    n_segment_samples = floor(classifier.cfg.segment_time * classifier.cfg.fs);
    n_segments = floor(n_samples / n_segment_samples);
    n_classes = size(classifier.stim.Mus, 3);
    
    % Prepare results structure
    labels = nan(n_trials, 1);
    results.score = zeros(n_trials, 1);
    results.scores = zeros(n_trials, n_classes);
    results.time = zeros(n_trials, 1);
    
    % Loop over trials
    for i_trial = 1:n_trials
        
        % Loop over segments
        for i_segment = 1:n_segments
            
            % Classify trial up to current segment
            x = X(:, 1:i_segment * n_segment_samples, i_trial);
            [label, result, classifier] = jt_tmc_apply_single_trial(classifier, x);
            
            % Stop trial and save results if classified
            if ~isnan(label)
                labels(i_trial) = label;
                results.score(i_trial) = result.score;
                results.scores(i_trial, :) = result.scores;
                results.time(i_trial) = result.time;
                break;
            end
            
        end
        
    end
    