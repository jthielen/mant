function [classifier] = jt_tmc_view(classifier)
% [classifier] = jt_tmc_view(classifier)
% Plots the classifier: temporal filter, spatial filter, stopping margins,
% and estimated accuracy
%
% INPUT
%   classifier = [struct] classifier structure
%
% OUTPUT
%   classifier = [struct] classifier with (updated) handle

% Initialize figure
classifier.view = figure(8392);
set(classifier.view,...
    'name', sprintf('jt_tmc_view | %s', classifier.cfg.user),...
    'numbertitle', 'off', ...
    'toolbar', 'none', ...
    'menubar', 'none', ...
    'units', 'normalized', ...
    'position', [.2 .2 .6 .6], ...
    'color', [0.5 0.5 0.5], ...
    'visible', 'off');

colors = [...
    0 0.4470 0.7410;...
    0.8500 0.3250 0.0980;...
    0.9290 0.6940 0.1250;...
    0.4940 0.1840 0.5560;...
    0.4660 0.6740 0.1880;...
    0.3010 0.7450 0.9330;...
    0.6350 0.0780 0.1840;...
    ];

% Transient responses
subplot(2, 3, 1:2);
cla(gca);
if jt_exists_in(classifier, 'transients')
    lengths = floor(classifier.cfg.L * classifier.cfg.fs);
    n_transients = numel(lengths);
    if n_transients <= size(colors, 2)
        colors_ = colors;
    else
        colors_ = hsv(n_transients);
    end
    labels = cell(1, n_transients);
    hold on;
    for i_transient = 1:n_transients
        labels{i_transient} = num2str(i_transient);
        duration = 0:1 / classifier.cfg.fs:(lengths(i_transient) - 1) / classifier.cfg.fs;
        transient = classifier.transients(1 + sum(lengths(1:i_transient - 1)):sum(lengths(1:i_transient)), :);
        plot(duration, transient, 'color', colors_(i_transient, :), 'linewidth', 1.5);
    end
    set(gca, 'xlim', [0 max(classifier.cfg.L)]);
    legend(labels, 'location', 'NorthEast');
end
xlabel('time [sec]');
ylabel('amplitude [a.u.]');
set(gca, 'color', [.75 .75 .75], 'xgrid', 'on', 'ygrid', 'on', 'box', 'on');
title('transient responses');

% Spatial filter
subplot(2, 3, 3);
cla(gca);
if jt_exists_in(classifier, 'filter')
    jt_topoplot(mean(classifier.filter, 2), struct('capfile', classifier.cfg.capfile, 'electrodes', 'numbers'));
end
set(gca, 'color', [.75 .75 .75], 'box', 'on');
title('spatial filter');

% Stopping
subplot(2, 3, 4:5);
cla(gca);
if jt_exists_in(classifier, 'stopping')
    duration = classifier.cfg.segment_time:classifier.cfg.segment_time:classifier.cfg.max_time;
    switch classifier.cfg.method
        case {'fix', 'beta'}
        case {'static_max_itr', 'static_max_acc', 'static_tgt_acc'}
            stop = zeros(numel(duration), 1);
            stop(duration >= classifier.stopping.time) = 1;
            plot(duration, stop, '-b', 'linewidth', 1.5);
        case 'margin'
            plot(duration, classifier.stopping.margins, '-b', 'linewidth', 1.5);
        case {'bes0', 'bes1', 'bes2'}
            hold on;
            plot(duration, classifier.stopping.eta, '-k', 'linewidth', 1.5);
            plot(duration, classifier.stopping.alpha * classifier.stopping.b0, '-', 'linewidth', 1.5, 'color', colors(1, :));
            plot(duration, classifier.stopping.alpha * classifier.stopping.b0 - classifier.stopping.s0, '--', 'linewidth', 1.5, 'color', colors(1, :));
            plot(duration, classifier.stopping.alpha * classifier.stopping.b0 + classifier.stopping.s0, '--', 'linewidth', 1.5, 'color', colors(1, :));
            plot(duration, classifier.stopping.alpha * classifier.stopping.b1, '-', 'linewidth', 1.5, 'color', colors(2, :));
            plot(duration, classifier.stopping.alpha * classifier.stopping.b1 - classifier.stopping.s1, '--', 'linewidth', 1.5, 'color', colors(2, :));
            plot(duration, classifier.stopping.alpha * classifier.stopping.b1 + classifier.stopping.s1, '--', 'linewidth', 1.5, 'color', colors(2, :));
        otherwise
            error('Unknown stopping method: %s', classifier.cfg.method)
    end
end
xlabel('trial duration [sec]');
ylabel('parameter');
set(gca, 'color', [.75 .75 .75], 'xgrid', 'on', 'ygrid', 'on', 'box', 'on');
title(sprintf('stopping parameters (%s)', classifier.cfg.method), 'Interpreter', 'none');

% Classification performance
subplot(2, 3, 6);
cla(gca);
set(gca, 'xlim', [0 1], 'ylim', [0 1]);
if jt_exists_in(classifier, 'accuracy') && jt_exists_in(classifier.accuracy, 'accuracy')
    text(.1, .5, sprintf(...
        ['P = %.2f %% \n' ...
         'T = %.2f sec \n' ...
         'N = %d \n' ...
         'ITI = %.2f sec\n' ...
         'ITR = %.2f bits/min'], ...
        classifier.accuracy.accuracy * 100, ...
        classifier.accuracy.time, ...
        size(classifier.stim.Mus, 3), ...
        classifier.cfg.inter_trial_time, ...
        classifier.accuracy.itr), ... 
        'fontsize', 16);
end
set(gca, 'color', [.75 .75 .75], 'xtick', [], 'ytick', [], 'box', 'on');
if classifier.cfg.verbosity == 2
    title('performance estimate (on train data)');
elseif classifier.cfg.verbosity == 3
    title(sprintf('performance estimate (%d-fold cv)', classifier.cfg.n_folds));
else
    title('performance estimate');
end

% Visualize figure
set(classifier.view, 'visible', 'on');
drawnow;
