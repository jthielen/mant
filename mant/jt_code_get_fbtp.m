function [fbtp] = jt_code_get_fbtp(m, verbosity)
% [fbtp] = jt_code_get_fbtp(m, verbosity)
% Generates the full set of feedback tap positions of degree (m) of the 
% primitive polynomial.
%
% The feedback tap positions should be connected according to a primitive 
% polynomial (it cannot be factored (i.e. it is prime), and it is a factor 
% of (i.e. can evenly divide) x^(n)+1, where n=2^(m)+1 (the length of the 
% m-sequence). All primitive polynomials that have a degree equal to m are 
% considered to be fine for m-sequence generation.
%
% INPUT
%   m         = [int] degree of polynomial
%   verbosity = [str] verbosity level (0)
%
% OUTPUT
%   fbtp = [cell] cell array of all posible primitive polynomials for m.

if nargin < 2 || isempty(verbosity); verbosity = 0; end

% Check if toolbox is available
if isempty(which('primpoly.m'))
    error('Communications System Toolbox is required!'); 
end

% Find all primitive polynomials
poly = gfprimfd(m, 'all');
n_poly = size(poly, 1);

% Convert to Feedback Tap Positions
set = repmat((m:-1:1), [n_poly 1]) .* poly(:, 1:end-1);

% Re-arrange small to large
[~, idx] = sort(sum(set, 2), 'ascend');
set = set(idx, :);

% Convert to cell structure
fbtp = cell(n_poly, 1);
for i = 1:n_poly
    [~, ~, fbtp{i}] = find(set(i, :));
end

% Print
if verbosity > 0
    for i = 1:numel(fbtp)
        fprintf('[')
        for j = 1:numel(fbtp{i})
            fprintf([num2str(fbtp{i}(j)) ' '])
        end
        fprintf('\b] ');
    end
    fprintf('\n')
end
