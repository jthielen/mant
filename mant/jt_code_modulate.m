function [codes] = jt_code_modulate(codes)
% [codes] = jt_code_modulate(codes)
% Modulates codes.
%
% INPUT
%   codes  = [m n] original codes of shape bits by codes
%
% OUTPUT
%   codes = [2*m n] modulated codes of shape bits by codes

% Upsample
codes = jt_upsample(codes, 2);

% Generate bit-clock
clock = zeros(size(codes));
clock(1:2:end, :) = 1;

% Modulate
codes = xor(codes, clock);
