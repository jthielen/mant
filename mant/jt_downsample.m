function [x] = jt_downsample(x, rate, dim)
% [x] = jt_downsample(x, rate, dim)
% Downsamples a cell, array or matrix with a certain rate directly 
% Note: no filters applied (i.e., decimation)
%
% INPUT
%   x    = [N-D]  matrix to be downsampled
%        = [cell] cell to be downsampled
%   rate = [int]  rate at which to downsample 
%   dim  = [int]  dimension along which to downsample (2)
% 
% OUTPUT
%   x = [N-D]  downsampled matrix
%       [cell] cell with individual downsampled variables

if nargin<3||isempty(dim); dim=2; end

% Downsample
if iscell(x)
    for i = 1:numel(x)
        x{i} = jt_downsample(x{i}, rate, dimension);
    end
else
    if dim == 2
        x = x(1:rate:end, :);
    else
        x = x(:, 1:rate:end);
    end
end
