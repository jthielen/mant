function [flag, rho] = jt_code_check_crosscorrelation(m, codes1, codes2)
% [flag, rho] = jt_code_check_crosscorrelation(m, codes1, codes2)
% Checks whether the cross-correlation of a variable is three valued.
%
% INPUT
%   m      = [int] register length
%   codes1 = [m p] codes of shape bits by codes
%   codes2 = [m q] codes of shape bits by codes (codes1)
%
% OUTPUT
%   flag = [int]  1 if 3-valued, otherwise 0
%   vals = [flt] unique cross-correlation values  

if nargin < 3 || isempty(codes2); codes2 = codes1; end

% Compute unique unnormalized correlation values
rho = zeros(size(codes1, 2), size(codes2, 2), size(codes1, 1));
for i = 1:size(codes1, 1)
    rho(:, :, i) = codes1' * circshift(codes2, i-1, 1);
end
rho = round(rho, 5);
rho = sort(unique(rho));

% Should be three-valued excluding the zero-lag auto-correlation
if numel(rho) ~= 4
    flag = 0; 
    return; 
end

% Check 3-valued cross-correlation (and auto-correlation)
if mod(m, 2) == 0 && ...
   all(rho == [-(2^((m + 2) / 2) + 1) -1 (2^((m + 2) / 2) - 1) 2^m-1]) || ...
   mod(m, 2) == 1 && ...
   all(rho == [-(2^((m + 1) / 2) + 1) -1 (2^((m + 1) / 2) - 1) 2^m-1])
    flag = 1;
else
    flag = 0;
end
