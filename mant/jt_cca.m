function [Wx, Wy, r] = jt_cca(X, Y, lx, ly, k, iCxx, iCyy)
% [Wx, Wy, r] = jt_cca(X, Y, lx, ly, k, iCxx, iCyy)
% Performs canonical correlation analysis.
%
% INPUT
%   X      = [m p] p variables of m samples
%          = [p q] covariance matrix of X and Y, given Y=[];
%   Y      = [m q] q variables of m samples
%   lx     = [flt] regularization X, range [0 1], 0 to suppress (1)
%            [p 1] regularization defined for each sample of X
%   ly     = [flt] regularization Y, range [0 1], 0 to suppress (1)
%            [q 1] regularization defined for each sample of Y
%   k      = [int] number of component ('all')
%          = [1 n] indexes of components
%          = [str] 'all' components
%   iCxx   = [p p] precomputed inverse covariance of X
%   iCyy   = [q q] precomputed inverse covariance of Y
%
% OUTPUT
%   Wx = [p k] k components with coefficients for X
%   Wy = [q k] k components with coefficients for Y
%   r  = [k 1] k canonical correlations between XWx and YWy

if nargin < 3 || isempty(lx); lx = 1; end; lx = lx(:);
if nargin < 4 || isempty(ly); ly = 1; end; ly = ly(:);
if nargin < 5 || isempty(k); k = 'all'; end
if nargin < 6; iCxx = []; end
if nargin < 7; iCyy = []; end

% Covariances
if isempty(Y)
    C = X;
    p = numel(lx);
else
    p = size(X, 2);
    X = bsxfun(@minus, X, mean(X, 1));
    Y = bsxfun(@minus, Y, mean(Y, 1));
    C = cov([X Y]);
end
Cxx = C(1:p,1:p) + diag(lx);
Cxy = C(1:p, p+1:end);
Cyy = C(p+1:end, p+1:end) + diag(ly);

% Inversion
if isempty(iCxx)
    iCxx = real(Cxx^(-1/2));
end
if isempty(iCyy)
    iCyy = real(Cyy^(-1/2));
end

% SVD
[U, S, V] = svd(iCxx * Cxy * iCyy, 0);

% Canonical correlations
r = diag(S);

% Canonical coefficients
Wx = iCxx * U;
Wy = iCyy * V;
if ~strcmp(k, 'all')
    Wx = Wx(:, k);
    Wy = Wy(:, k);
    r = r(k);
end
