function [T] = jt_reconvolution_compose(M, R)
% [T] = jt_reconvolution_compose(M, R)
%
% INPUT
%   M = [e m n] structure matrices of events by samples by classes
%   R = [e 1]   transient responses shared over classes
%       [e n]   transient response for each class individually
%
% OUTPUT
%   T = [m n] predicted responses of samples by classes

[e, m, n] = size(M);

if size(R, 2) == 1
    % Simple product (though 3D M)
    T = reshape(R' * reshape(M, [e m * n]), [m n]);
else
    % Loop over individual classes (as they have individual transients)
    T = zeros(m, n);
    for i = 1:n
        T(:, i) = R(:, i)' * M(:, :, i);
    end
end
