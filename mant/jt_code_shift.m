function [codes] = jt_code_shift(code, lag, rearange)
% [codes] = jt_code_modulate(codes, method)
% Modulates codes.
%
% INPUT
%   code = [m 1] original code of shape bits by code
%   lag  = [int] shift lag (1)
%
% OUTPUT
%   codes = [m n] shifted codes of shape bits by codes

% Defaults
if nargin < 2 || isempty(lag); lag = 1; end
if nargin < 3 || isempty(rearange); rearange = []; end

% Compute number of shifts
shifts = 0:lag:size(code, 1);

% Generate shifted codes
codes = zeros(size(code, 1), numel(shifts));
codes(:, 1) = code;
for i = 1:numel(shifts)
    codes(:, i) = circshift(code, shifts(i));
end

% Rearrange
if ~isempty(rearange)
    
    idx = 1:size(codes, 2);
    idx = reshape(idx, rearange(2), rearange(1))';
    idx = idx(:);
    codes = codes(:, idx);

end
