function [W, R, model] = jt_reconvolution_decompose(X, y, M, cfg, model)
% [W, R, model] = jt_reconvolution_decompose(X, y, M, cfg, model)
%
% INPUT
%   X = [c m k] data matrix of channels by samples by trials
%   y = [k 1]   label of each of the trials
%   M = [e m k] structure matrices of events by samples by trials
%
%   cfg = [struct] configuration structure:
%       .fs         = [int]  data sample frequency in Hertz (256)
%       .L          = [1 r]  length of transient responses in seconds (0.3)
%       .component  = [int]  CCA component to use (1)
%       .lx         = [flt]  regularization on data.X (1)
%                     [1 c]  regularization on data.X for each sample
%                     [str]  regularization on data.X with taper
%       .ly         = [flt]  regularization on Y (1)
%                     [1 L]  regularization on Y for each sample
%                     [str]  regularization on data.X with taper
%       .lxamp      = [flt]  amplifier for lx regularization penalties, i.e., maximum penalty (0.1)
%       .lyamp      = [flt]  amplifier for ly regularization penalties, i.e., maximum penalty (0.01)
%       .lyperc     = [flt]  relative parts of the taper that is regularized (0.2)
%       .modelonset = [bool] whether or not to model the onset, uses L(end) as length (false)
%   model = [struct] incremental covariance model
%
% OUTPUT
%   R     = [e 1]    concattenated transient responses of e=sum(L) samples
%   W     = [c 1]    spatial filter to be applied to data [channels components]
%   model = [struct] incremental covariance model

if nargin < 5; model = []; end
[c, m, k] = size(X);
e = size(M, 1);
if isempty(y)
    n = size(M, 3);
else
    n = 1;
end
fs = jt_parse_cfg(cfg, 'fs', 256);
L = jt_parse_cfg(cfg, 'L', 0.3);
component = jt_parse_cfg(cfg, 'component', 1);
lx = jt_parse_cfg(cfg, 'lx', 1);
ly = jt_parse_cfg(cfg, 'ly', 1);
lxamp = jt_parse_cfg(cfg, 'lxamp', 0.1);
lyamp = jt_parse_cfg(cfg, 'lyamp', 0.01);
lyperc = jt_parse_cfg(cfg, 'lyperc', 0.2);
modelonset = jt_parse_cfg(cfg, 'modelonset', false);
if isscalar(L) && floor(L * fs) ~= e; L = L * ones(1, floor(e / (L * fs))); end

% Regularization
lx = jt_regularization(lx, c, false, lxamp);
ly = jt_regularization(ly, floor(L * fs), modelonset, lyamp, lyperc);

% Initialize model
if isempty(model)
    model.count = 0;
    model.avg = zeros(c+e, n);
    model.cov = zeros(c+e, c+e, n);
elseif isempty(y) && size(model.avg, 2) == 1
    model.avg = repmat(model.avg, [1 n]);
    model.cov = repmat(model.cov, [1 1 n]); 
end

% Create data matrices
if isempty(y)
    X = permute(X, [2 1]);  % [m c]
    M = permute(M, [2 1 3]);  % [m e n]
else
    X = reshape(X, [c m * k])';  % [m*k c]
    M = reshape(M(:, :, y), [e m * k])';  % [m*k e]
end

% Decompose
W = zeros(c, n);
R = zeros(e, n);
for i = 1:n

    % Update covariance
    [model.avg(:, i), model.cov(:, :, i), tmpcount] = jt_covariance_incremental([X M(:, :, i)], model.avg(:, i)', model.cov(:, :, i), model.count);

    % Invert data covariance
    if i == 1
        Cxx = model.cov(1:c, 1:c) + diag(lx);
        iCxx = real(Cxx^(-1/2));
    end

    % CCA
    [W(:, i), R(:, i)] = jt_cca(model.cov(:, :, i), [], lx, ly, component, iCxx);
end

% Set data count
model.count = tmpcount;
