function [eta, alpha, sigma, b0, b1, s0, s1, pf, pm] = jt_stopping_bayes(X, y, T, cfg)
% [eta] = jt_stopping_bayes(X, y, T, cfg)
% Learn stopping boundary for dynamic stopping.
%
% INPUT
%   X = [m k] data of samples by trials
%   y = [k 1] labels for each trial
%   T = [m n] templates of samples by classes
%   cfg = [struct] configuration structure
%       .segmentlength  = [int]  segment length samples (100)
%       .cr             = [flt]  Bayes criterion
%
% OUTPUT
%   eta = [1 n] stopping threshold

% Defaults
if size(T, 1) ~= size(X, 1); error('X and T have different number of samples'); end
if nargin < 4 || isempty(cfg); cfg = []; end
segmentlength = jt_parse_cfg(cfg, 'segmentlength', 100);  % length of segment in samples
cr = jt_parse_cfg(cfg, 'cr', 1.0);
[n_samples, n_classes] = size(T);

% Obtain alpha from least squares optimization
[alpha, ~, residuals] = lsqnonneg(reshape(T(:, y), [], 1), reshape(X, [], 1));

% Fit Gaussian on residuals
pd = fitdist(residuals, 'Normal');
sigma = pd.sigma;

% Calculate b0, b1, s0, s1
n_segments = floor(n_samples / segmentlength);
b0 = zeros(n_segments, 1);
b1 = zeros(n_segments, 1);
s0 = zeros(n_segments, 1);
s1 = zeros(n_segments, 1);
for i_segment = 1:n_segments
    idx = 1:floor(i_segment * segmentlength);
    inner = T(idx, :)' * T(idx, :);
    inner_xy = inner(eye(n_classes) == 0);
    inner_xx = inner(eye(n_classes) == 1);
    b0(i_segment) = mean(inner_xy);
    b1(i_segment) = mean(inner_xx);
    %s0(i_segment) = sqrt(b1(i_segment) * sigma^2 + std(alpha * inner_xy).^2); 
    s0(i_segment) = sqrt(b1(i_segment) * sigma^2 + mean((alpha * inner_xy - alpha * b0(i_segment)).^2));
    %s1(i_segment) = sqrt(b1(i_segment) * sigma^2 + std(alpha * inner_xx).^2);
    s1(i_segment) = sqrt(b1(i_segment) * sigma^2 + mean((alpha * inner_xx - alpha * b1(i_segment)).^2));
end

% Calculate eta
a = s1.^2 - s0.^2;
b = -2 * alpha * (s1.^2 .* b0 - s0.^2 .* b1);
c = -alpha^2 * (s1.^2 .* b0.^2 + s0.^2 .* b1.^2) + 2 * s0.^2 .* s1.^2 .* log(s0 ./ (s1 .* (n_classes - 1) .* cr));
eta = (-b + sqrt(max(b.^2 - 4.*a.*c, 0))) ./ (2 * a);

% Calculate predicted error vectors (corrected for multiple comparisons)
pf = ((n_classes - 1) / n_classes) * (1 - normcdf(eta, alpha * b0, s0));
pf = 1 - (1 - pf).^n_classes;
pm = (1 / n_classes) * normcdf(eta, alpha * b1, s1);
pm = 1 - (1 - pm).^n_classes;
