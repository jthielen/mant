function [code] = jt_code_make_mm_sequence(m, v)
% [code] = jt_code_make_mm_sequence(m, v)
% Generates a modulated maximum length sequence.
% 
% INPUT
%   m = [int] register length (6)
%   v = [1 p] array of p feedback tab points ([6 1])
% 
% OUTPUT
%   code = [2*(2^m-1) 1] m-sequence of shape bits by code

% Check input
if nargin < 1 || isempty(m); m = 6; end
if nargin < 2 || isempty(v); v = [6 1]; end

% Generate mls
code = jt_code_make_m_sequence(m, v);

% Modulate mls
code = jt_code_modulate(code);
