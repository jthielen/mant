function [flag] = jt_exists_in(cfg, fields)
% [flag] = jt_exists_in(classifier, fields)
% 

if ~iscell(fields)
    fields = {fields}; 
end

if isempty(cfg)
    flag = false;
else
    flag = true;
    for i = 1:numel(fields)
        if ~isfield(cfg, fields{i}) || isempty(cfg.(fields{i}))
            flag = false;
            return;
        end
    end
end
