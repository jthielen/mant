%--------------------------------------------------------------------------
% README
%
% This script works with the "reconvolution" noise-tagging dataset as 
% published in and downloadable from: https://doi.org/10.34973/1ecz-1232
%
% The accompanying article that produced these data is:
% Thielen, J., Van Den Broek, P., Farquhar, J., & Desain, P. (2015). 
% Broad-Band visually evoked potentials: re(con)volution in brain-computer 
% interfacing. PLOS ONE, 10(7), e0133797. DOI: 
% https://doi.org/10.1371/journal.pone.0133797
%
% To use this script:
%   1. Make sure you have the MaNT library:
%      https://gitlab.socsci.ru.nl
%   2. Make sure you have the FieldTrip library: 
%      https://www.fieldtriptoolbox.org/
%   3. Change the paths to the libraries on your device in the "Add 
%      libraries" section.
%   4. Change the path to the dataset on your device in the "Data path"
%      section.
%   5. Optionally change some of the constants in the "Constants" section.
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add libraries
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant')));

% Add FieldTrip library
addpath(fullfile('~', 'fieldtrip'));
ft_defaults;

%--------------------------------------------------------------------------
% Data path
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'thielen2015');

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Data files
% subjects = {'sub-01'};
n_subjects = 12;
subjects = cell(n_subjects, 1);
for i_subject = 1:n_subjects
    subjects{i_subject} = sprintf('sub-%02d', i_subject);
end
blocks = {'train', 'test_sync_1', 'test_sync_2', 'test_sync_3'};

fs = 360;
fr = 120;
hpfreq = 5;
lpfreq = 35;
%bsfreq = [48 52];
trial_time = 4.2;

%--------------------------------------------------------------------------
% Read data
%--------------------------------------------------------------------------

n_subjects = numel(subjects);
n_blocks = numel(blocks);

% Loop over subjects
for i_subject = 1:n_subjects
    subject = subjects{i_subject};
    
    fprintf('\n\n\n---------------------------------------------\n');
    fprintf('\nsubject: %s\n\n', subject)

    % Pre-allocate data structure
    data = cell(n_blocks, 1);
    labels = cell(n_blocks, 1);
    codes = cell(n_blocks, 1);
    
    % Loop over blocks
    for i_block = 1:n_blocks
        block = blocks{i_block};
        
        % Read labels
        in = load(fullfile(root, 'sourcedata', subject, block, sprintf('%s_%s.mat', subject, block)));
        labels{i_block} = in.labels(:);
        
        % Read codes
        codes{i_block} = jt_upsample(in.codes(:, in.subset(in.layout)), fs / fr);

        % Find data
        dataset = fullfile(root, 'sourcedata', subject, block, sprintf('%s_%s.gdf', subject, block));
        
        % Read data
        cfg = [];
        cfg.dataset = dataset;
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg);

        %------------------------------------------------------------------
        % Linear detrend
        %cfg = [];
        %cfg.detrend = 'yes';
        %data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % Common average reference (CAR)
        %cfg = [];
        %cfg.reref = 'yes';
        %cfg.refchannel = 2:65;
        %data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % High-pass
        cfg = [];
        cfg.hpfilter = 'yes';
        cfg.hpfreq = hpfreq;
        cfg.hpfiltord = 6;
        cfg.hpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % Low-pass
        cfg = [];
        cfg.lpfilter = 'yes';
        cfg.lpfreq = lpfreq;
        cfg.lpfiltord = 6;
        cfg.lpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % Band-stop (notch)
        %cfg = [];
        %cfg.bsfilter = 'yes';
        %cfg.bsfreq = bsfreq;
        %cfg.bsfilttype = 'but';
        %cfg.feedback = 'no';
        %data{i_block} = ft_preprocessing(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Slice data
        % N.B. +1 second before and after for filter artefacts 
        cfg = [];
        cfg.dataset = dataset;
        cfg.trialfun = 'ft_trialfun_general';
        cfg.trialdef.eventtype = 'STATUS';
        cfg.trialdef.eventvalue = 1:2;  % 1 train trials, 2 test trials
        cfg.trialdef.prestim = 1;  
        cfg.trialdef.poststim = trial_time + 1;
        cfg = ft_definetrial(cfg);
        cfg.feedback = 'no';
        data{i_block} = ft_redefinetrial(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Select channels
        cfg = []; 
        cfg.channel = 2:65;  % standard 64 cap layout
        cfg.feedback = 'no';
        data{i_block} = ft_selectdata(cfg, data{i_block});

        %------------------------------------------------------------------
        % Downsample
        cfg = [];
        cfg.resamplefs = fs;
        cfg.feedback = 'no';
        data{i_block} = ft_resampledata(cfg, data{i_block});

    end

    % Append blocks
    train_data = data{1};
    test_data = ft_appenddata([], data{2:end});
    train_labels = labels{1};
    test_labels = cat(1, labels{2:end});
    train_codes = codes{1};
    test_codes = codes{2};
    
    % Extract data
    % N.B. remove 1 second before and after trial for filter artefacts
    n_samples = floor(trial_time * fs);
    X_train = cat(3, train_data.trial{:});
    X_test = cat(3, test_data.trial{:});
    X_train = X_train(:, fs + 1:fs + n_samples, :);
    X_test = X_test(:, fs + 1:fs + n_samples, :);
    
    % Change types
    X_train = double(X_train);
    X_test = double(X_test);
    y_train = uint8(train_labels);
    y_test = uint8(test_labels);
    V = logical(train_codes);
    U = logical(test_codes);
    
    % Print sizes
    fprintf('\tX train: %d x %d x %d \n', size(X_train, 1), size(X_train, 2), size(X_train, 3));
    fprintf('\ty train: %d \n', numel(y_train));
    fprintf('\tV: %d x %d \n', size(V, 1), size(V, 2));
    fprintf('\tX test: %d x %d x %d \n', size(X_test, 1), size(X_test, 2), size(X_test, 3));
    fprintf('\ty test: %d \n', numel(y_test));
    fprintf('\tU: %d x %d \n', size(U, 1), size(U, 2));
    
    % Create output folder
    if ~exist(fullfile(root, 'derivatives', subject), 'dir')
        mkdir(fullfile(root, 'derivatives', subject));
    end

    % Save data
    save(fullfile(root, 'derivatives', subject, sprintf('%s_gdf.mat', subject)), ...
        'X_train', 'y_train', 'V', 'X_test', 'y_test', 'U', 'fs');
    
end
