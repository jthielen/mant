%--------------------------------------------------------------------------
% README
%
% This script works with the "reconvolution" noise-tagging dataset as 
% published in and downloadable from: https://doi.org/10.34973/1ecz-1232
%
% The accompanying article that produced these data is:
% Thielen, J., Van Den Broek, P., Farquhar, J., & Desain, P. (2015). 
% Broad-Band visually evoked potentials: re(con)volution in brain-computer 
% interfacing. PLOS ONE, 10(7), e0133797. DOI: 
% https://doi.org/10.1371/journal.pone.0133797
%
% Note, this script seeks to replicate the analysis as published in the
% referenced article. 
%
% Note, replication also depends on the preprocessing of the data, as
% performed in the jt_thielen2015_preprocessing.m script.
%
% To use this script:
%   1. Make sure you have ran the jt_thielen2015_preprocessing.m script. 
%   2. Change the paths to the MaNT library on your device in the "Add 
%      libraries" section.
%   3. Change the path to the dataset on your device in the "Data path"
%      section.
%   4. Optionally change some of the parameters throughout the script 
%      (e.g., the cfg variable for the classifier).
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add library
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant', 'mant')));

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'thielen2015');

% Participants
n_subjects = 12;

% Classifier
fs = 360;
transient_length = 0.15;
lx = 0.9;
ly = 0.9;

%--------------------------------------------------------------------------
% Analysis
%--------------------------------------------------------------------------

accuracy = zeros(n_subjects, 1);
filters = zeros(n_subjects, 64);
transients = zeros(n_subjects, floor(2 * transient_length * fs));
for i_subject = 1:n_subjects
    subject = sprintf('sub-%02d', i_subject);
    
    % Load data
    data = load(fullfile(root, 'derivatives', subject, ...
        sprintf('%s_gdf.mat', subject)));
    data.V = repmat(data.V, 4, 1);
    data.U = repmat(data.U, 4, 1);
    n_channels = size(data.X_train, 1);
    n_codes = size(data.V, 2);
    
    % Create structure matrices
    Mv = jt_reconvolution_structure_matrix(data.V, struct(...
        'L', floor(transient_length * data.fs), ...
        'event', 'duration', ...
        'modelonset', false));
    Mu = jt_reconvolution_structure_matrix(data.U, struct(...
        'L', floor(transient_length * data.fs), ...
        'event', 'duration', ...
        'modelonset', false));
    
    % Deconvolve transient responses
    S = reshape(data.X_train, n_channels, []);
    M = reshape(Mv(:, :, data.y_train), size(Mv, 1), []);
    R = S / M;
    
    % Predict templates
    Tv = reshape(R * reshape(Mv, size(Mv, 1), []), n_channels, [], n_codes);
    Tu = reshape(R * reshape(Mu, size(Mv, 1), []), n_channels, [], n_codes);
    
    % CCA
    T = reshape(Tv(:, :, data.y_train), n_channels, []);
    % [Wx, Wt] = canoncorr(S', T');
    % Wx = Wx(:, 1);
    % Wt = Wt(:, 1);
    [Wx, Wt] = jt_cca_qr(S', T', lx, ly, 1);

    % Save filter and transients
    filters(i_subject, :) =  Wx;
    transients(i_subject, :) = Wt' * R;
    
    % Spatially filter data
    X_test = reshape(Wx' * reshape(data.X_test, n_channels, []), ...
        [], size(data.X_test, 3));
    Tu = reshape(Wt' * reshape(Tu, n_channels, []), [], n_codes);
    
    % Classify data
    rho = jt_correlation(X_test, Tu);
    [val, yh] = max(rho, [], 2);
    
    % Compute accuracy
    accuracy(i_subject) = mean(yh == data.y_test);

end

% Align responses
avg = transients(1, :);
for i_subject = 2:n_subjects
    rho = corrcoef(vertcat(avg, transients(i_subject, :))');
    mul = sign(rho(1, 2));
    filters(i_subject, :) = mul * filters(i_subject, :);
    transients(i_subject, :) = mul * transients(i_subject, :);
    avg = mean(transients(1:i_subject, :), 1);
end

%--------------------------------------------------------------------------
% Visualization
%--------------------------------------------------------------------------

% Plot accuracy
figure();
hold on;
bar(100 * accuracy);
plot([1 n_subjects], 100 * [mean(accuracy) mean(accuracy)], '--k');
xlabel('participant [#]');
ylabel('accuracy [%]');
title(sprintf('average: %.1f%%', 100 * mean(accuracy)));

% Plot transients
figure();
subplot(1, 2, 1);
plot(0:1/fs:transient_length-1/fs, transients(:, 1:floor(transient_length * fs)));
xlabel("time [sec]");
ylabel('amplitude [a.u.]');
title('short flash');
subplot(1, 2, 2);
plot(0:1/fs:transient_length-1/fs, transients(:, 1+floor(transient_length * fs):end));
xlabel("time [sec]");
ylabel('amplitude [a.u.]');
title('long flash');

% Plot filters
figure();
for i_subject = 1:n_subjects
    subplot(3, 4, i_subject);
    jt_topoplot(filters(i_subject, :), struct('capfile', 'nt_cap64.loc'));
    title(sprintf('sub-%02d', i_subject));
end
