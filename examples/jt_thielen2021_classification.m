%--------------------------------------------------------------------------
% README
%
% This script works with the "zero-training" noise-tagging dataset as 
% published in and downloadable from: https://doi.org/10.34973/9txv-z787
%
% The accompanying article that produced these data is:
% Thielen, J., Marsman, P., Farquhar, J., & Desain, P. (2021). From full 
% calibration to zero training for a code-modulated visual evoked 
% potentials for brain–computer interface. Journal of Neural Engineering, 
% 18(5), 056007. DOI: https://doi.org/10.1088/1741-2552/abecef
%
% Note, this script does substantially deviate from the analysis as
% performed in the referenced article. 
%
% To use this script:
%   1. Make sure you have ran the jt_thielen2021_preprocessing.m script. 
%   2. Change the paths to the MaNT library on your device in the "Add 
%      libraries" section.
%   3. Change the path to the dataset on your device in the "Data path"
%      section.
%   4. Optionally change some of the parameters throughout the script 
%      (e.g., the cfg variable for the classifier).
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add library
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant')));

%--------------------------------------------------------------------------
% Data path
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'thielen2021');

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Data files
subject = 'sub-01';

%--------------------------------------------------------------------------
% Load data
%--------------------------------------------------------------------------

data = load(fullfile(root, 'derivatives', 'offline', subject, ...
    sprintf('%s_gdf.mat', subject)));
data.U = data.V;

%--------------------------------------------------------------------------
% Supervisedly trained classifier fixed length trials
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', 'nt_cap8.loc', 'fs', data.fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'fix', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * data.fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping margin
%--------------------------------------------------------------------------

% Settings
trial_time = 31.5;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', 'nt_cap8.loc', 'fs', data.fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'margin', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * data.fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping beta
%--------------------------------------------------------------------------

% Settings
trial_time = 31.5;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', 'nt_cap8.loc', 'fs', data.fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'beta', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * data.fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping bayes
%--------------------------------------------------------------------------

% Settings
trial_time = 31.5;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', 'nt_cap8.loc', 'fs', data.fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'bes0', 'metric', 'inner', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * data.fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Zero training classifier
%--------------------------------------------------------------------------

% Settings
trial_time = 31.5;
segment_time = 0.5;
cfg = struct( ...
    'verbosity', 0, 'n_folds', 5, 'user', subject, ...
    'capfile', 'nt_cap8.loc', 'fs', data.fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'beta', 'zerotraining', true, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Init classifier
tmp = data;
tmp.X = [];
tmp.y = [];
classifier = jt_tmc_train(tmp, cfg);

% Zero training
n_trials = size(data.X, 3);
n_segments = floor(size(data.X, 2) / data.fs / segment_time);
accuracy = zeros(n_trials, 1);
duration = zeros(n_trials, 1);
for i = 1:n_trials
    for j = 1:n_segments
       
        % Apply classifier
        [label, ~, classifier] = jt_tmc_apply(classifier, ...
            data.X(:, 1:floor(j * segment_time * data.fs), i));
        
        % Save results if classified
        if ~isnan(label)
            accuracy(i) = label == data.y(i);
            duration(i) = j * segment_time;
            break;
        end
        
    end
end

% View classifier
classifier.accuracy.p = mean(accuracy);
classifier.accuracy.t = mean(duration);
classifier.accuracy.itr = jt_itr(size(data.V, 2), ...
    classifier.accuracy.p, classifier.accuracy.t);
jt_tmc_view(classifier);

% Visualize performance
figure();
subplot(2, 1, 1);
hold on;
plot(100 * accuracy);
plot([1 numel(accuracy)], 100 * [mean(accuracy) mean(accuracy)], '--k');
xlabel('trial [#]');
ylabel('accuracy [%]');
title(sprintf('average: %.1f%%', 100 * mean(accuracy)));
subplot(2, 1, 2);
hold on;
plot(duration);
plot([1 numel(duration)], [mean(duration) mean(duration)], '--k');
xlabel('trial [#]')
ylabel('duration [sec]');
title(sprintf('average: %.1f sec', mean(duration)));
sgtitle('Zero-training');

%--------------------------------------------------------------------------
% Cross-validation all participants 
%--------------------------------------------------------------------------

% Reset workspace
clear variables;
close all;
clc;
addpath(genpath(fullfile('~', 'mant')));
root = fullfile('~', 'data', 'thielen2021');

% Settings
n_subjects = 30;
n_folds = 5;
n_trials = 100;
trial_time = 4.2;
fs = 120;
segment_time = 0.5;
cfg = struct( ...
    'verbosity', 1, 'n_folds', 5, 'user', 'sub', ...
    'capfile', 'nt_cap8.loc', 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'fix', 'zerotraining', false, ...
    'L', 0.2, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Folds
folds = repmat((1:n_folds), floor(n_trials / n_folds), 1);
folds = folds(:);

% Loop subjects
accuracy = zeros(n_subjects, n_folds);
for i_subject = 1:n_subjects
    subject = sprintf('sub-%02d', i_subject);

    % Load data
    tmp = load(fullfile(root, 'derivatives', 'offline', subject, sprintf('%s_gdf.mat', subject)));
    data = struct();
    data.V = tmp.V;
    data.U = tmp.V;

    % Loop folds
    for i_fold = 1:n_folds
        cfg.user = sprintf('%s - fold-%02d', subject, i_fold);

        % Train classifier
        data.X = tmp.X(:, :, folds~=i_fold);
        data.y = tmp.y(folds~=i_fold);
        classifier = jt_tmc_train(data, cfg);
        
        % Apply classifier
        [labels, results] = jt_tmc_apply(classifier, tmp.X(:, :, folds==i_fold));
        accuracy(i_subject, i_fold) = 100 * mean(labels == tmp.y(folds==i_fold));

    end

end

% Add mean accuracy
accuracy = cat(1, accuracy, mean(accuracy, 1));

% Plot accuracy
figure();
hold on;
bar(1:n_subjects+1, mean(accuracy, 2));
plot([1, n_subjects+1], [mean(accuracy(:)), mean(accuracy(:))], 'k--');
er = errorbar(1:n_subjects+1, mean(accuracy, 2), std(accuracy, [], 2));
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
xlabel('subject');
ylabel('accuracy [%]');

% Print accuracy
for i_subject = 1:n_subjects+1
    if i_subject <= n_subjects
        fprintf('sub-%02d: %.1f +/- %.1f\n', i_subject, mean(accuracy(i_subject, :)), std(accuracy(i_subject, :)));
    else
        fprintf('avg: %.1f +/- %.1f\n', mean(accuracy(i_subject, :)), std(accuracy(i_subject, :)));
    end
end
