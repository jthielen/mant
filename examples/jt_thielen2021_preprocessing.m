%--------------------------------------------------------------------------
% README
%
% This script works with the "zero-training" noise-tagging dataset as 
% published in and downloadable from: https://doi.org/10.34973/9txv-z787
%
% The accompanying article that produced these data is:
% Thielen, J., Marsman, P., Farquhar, J., & Desain, P. (2021). From full 
% calibration to zero training for a code-modulated visual evoked 
% potentials for brain–computer interface. Journal of Neural Engineering, 
% 18(5), 056007. DOI: https://doi.org/10.1088/1741-2552/abecef
%
% To use this script:
%   1. Make sure you have the MaNT library:
%      https://gitlab.socsci.ru.nl
%   2. Make sure you have the FieldTrip library: 
%      https://www.fieldtriptoolbox.org/
%   3. Change the paths to the libraries on your device in the "Add 
%      libraries" section.
%   4. Change the path to the dataset on your device in the "Data path"
%      section.
%   5. Optionally change some of the constants in the "Constants" section.
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add libraries
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant', 'mant')));

% Add FieldTrip library
addpath(fullfile('~', 'fieldtrip'));
ft_defaults;

%--------------------------------------------------------------------------
% Data path
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'thielen2021');

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Data files
% subjects = {'sub-01'};
n_subjects = 30;
subjects = cell(n_subjects, 1);
for i_subject = 1:n_subjects
    subjects{i_subject} = sprintf('sub-%02d', i_subject);
end
blocks = {'block_1', 'block_2', 'block_3', 'block_4', 'block_5'};
codes_file = 'mgold_61_6521_flip_balanced_20.mat';

fs = 120;  % target EEG sampling rate
fr = 60;  % monitor refresh rate
hpfreq = 2;  % high-pass cut-off frequency
lpfreq = 30;  % low-pass cut-off frequency
trial_time = 31.5;  % trial duration

%--------------------------------------------------------------------------
% Read data
%--------------------------------------------------------------------------

% Load codes
in = load(fullfile(root, 'resources', codes_file));
codes = jt_upsample(in.codes, fs / fr);

n_subjects = numel(subjects);
n_blocks = numel(blocks);

% Loop over subjects
for i_subject = 1:n_subjects
    subject = subjects{i_subject};
    
    fprintf('\n\n\n---------------------------------------------\n');
    fprintf('\nsubject: %s\n\n', subject)

    % Pre-allocate data structure
    data = cell(n_blocks, 1);
    labels = cell(n_blocks, 1);
    
    % Loop over blocks
    for i_block = 1:n_blocks
        block = blocks{i_block};
        
        % Read labels
        in = load(fullfile(root, 'sourcedata', 'offline', subject, ...
            block, 'trainlabels.mat'));
        labels{i_block} = in.v(:);

        % Find data
        listing = dir(fullfile(root, 'sourcedata', 'offline', subject, ...
            block, sprintf('%s_*_%s_main_eeg.gdf', subject, block)));
        if ~numel(listing) == 1
            error('Found none or multiple files.');
        end
        dataset = fullfile(root, 'sourcedata', 'offline', subject, ...
            block, listing.name);
        
        % Read data
        cfg = [];
        cfg.dataset = dataset;
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg);
        
        %------------------------------------------------------------------
        % Find marker locations
        markers = find(data{i_block}.trial{1}(1, :) < 0);
        diffs = diff(markers);
        condition1 = diffs < 1.8 * data{i_block}.fsample;
        condition2 = diffs > 2.4 * data{i_block}.fsample;
        trials = [1 1 + find(condition1 | condition2)];
        onsets = markers(trials);
        
%         % Inspect markers
%         figure();
%         hold on;
%         plot(data{i_block}.trial{1}(1, :), 'k-');
%         plot(markers, zeros(size(markers)), 'r.', 'markersize', 20);
%         plot(onsets, zeros(size(onsets)), 'g.', 'markersize', 20);
%         title(sprintf('%s | %s', subject, block));

%         %------------------------------------------------------------------
%         % Common average reference
%         cfg = [];
%         cfg.reref = 'yes';
%         cfg.refchannel = 2:9;
%         data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % High-pass
        cfg = [];
        cfg.hpfilter = 'yes';
        cfg.hpfreq = hpfreq;
        cfg.hpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % Low-pass
        cfg = [];
        cfg.lpfilter = 'yes';
        cfg.lpfreq = lpfreq;
        cfg.lpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Slice data
        % N.B. +1 second before and after for filter artefacts 
        cfg = [];
        cfg.dataset = dataset;
        cfg.trialfun = 'ft_trialfun_general';
        cfg.trialdef.eventtype = 'STATUS';
        cfg.trialdef.eventvalue = 1;
        cfg.trialdef.prestim = 1;
        cfg.trialdef.poststim = 31.5 + 0.5;  % 0.5 because end of data
        cfg = ft_definetrial(cfg);
        cfg.feedback = 'no';
        data{i_block} = ft_redefinetrial(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Select trials and channels
        % N.B.: remove STATUS channel (marker channel)
        % N.B.: remove "in-between trials": there are 320 events, only 305 
        % found because of trial length. There is a pulse every 2.1 seconds
        % in a trial of 31.5 second, and one trailing pulse. Hence, skip 15 
        % i.e. take every 16th.
        cfg = []; 
        cfg.channel = 2:9;
        cfg.trials = trials; 
        cfg.feedback = 'no';
        data{i_block} = ft_selectdata(cfg, data{i_block});

        %------------------------------------------------------------------
        % Down-sample
        cfg = [];
        cfg.resamplefs = fs;
        cfg.feedback = 'no';
        data{i_block} = ft_resampledata(cfg, data{i_block});
        
        % Check data
        if numel(data{i_block}.trial) ~= 20
            error('Did not find 20 trials in this block!');
        end

    end

    % Append blocks
    data = ft_appenddata([], data{:});
    labels = cat(1, labels{:});
    
    % Extract data
    % N.B. remove 1 second before and after trial for filter artefacts
    n_samples = floor(trial_time * fs);
    X = cat(3, data.trial{:});
    X = X(:, fs + 1:fs + n_samples, :);
    
    % Change types
    X = double(X);
    y = uint8(labels);
    V = logical(codes);
    
    % Print sizes
    fprintf('\tX: %d x %d x %d \n', size(X, 1), size(X, 2), size(X, 3));
    fprintf('\ty: %d \n', numel(y));
    fprintf('\tV: %d x %d \n', size(V, 1), size(V, 2));
    
    % Create output folder
    if ~exist(fullfile(root, 'derivatives', 'offline', subject), 'dir')
        mkdir(fullfile(root, 'derivatives', 'offline', subject));
    end

    % Save data
    save(fullfile(root, 'derivatives', 'offline', subject, ...
        sprintf('%s_gdf.mat', subject)), 'X', 'y', 'V', 'fs');
    
end
