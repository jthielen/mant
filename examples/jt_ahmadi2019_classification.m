%--------------------------------------------------------------------------
% README
%
% This script works with the "sensor-tying" noise-tagging dataset as 
% published in and downloadable from: https://doi.org/10.34973/ehq6-b836
%
% Note, this data is not raw data, i.e., it is already preprocessed.
%
% The accompanying article that produced these data is:
% Ahmadi, S., Borhanazad, M., Tump, D., Farquhar, J., & Desain, P. (2019). 
% Low channel count montages using sensor tying for VEP-based BCI. Journal 
% of Neural Engineering, 16(6), 066038. DOI: 
% https://doi.org/10.1088/1741-2552/ab4057
%
% Note, this script does substantially deviate from the analysis as
% performed in the referenced article. 
%
% To use this script:
%   1. Note: the dataset provides data that is already preprocessed.
%   2. Change the paths to the MaNT library on your device in the "Add 
%      libraries" section.
%   3. Change the path to the dataset on your device in the "Data path"
%      section.
%   4. Optionally change some of the parameters throughout the script 
%      (e.g., the cfg variable for the classifier).
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add library
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant', 'mant')));

%--------------------------------------------------------------------------
% Data path
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'ahmadi2019');

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Data files
subject = 'S1';
session = 8;  % 8 or 32 (channels)

%--------------------------------------------------------------------------
% Load data
%--------------------------------------------------------------------------

in = load(fullfile(root, 'sourcedata', ...
    sprintf('%s-%d.mat', subject, session)));
data = in.data;

labels = unique(data.y);
for i = 1:numel(data.y)
    data.y(i) = find(labels == data.y(i));
end

data.V = data.V(1:3:end, labels);  % downsampled
data.U = data.V;

% Resample data to 120 hz
fs = in.classifier.fs;
data.X = resample(double(data.X), 120, fs, 'dimension', 2);
fs = 120;

if session == 8
    capfile = 'nt_cap8.loc';
else
    capfile = 'nt_cap32.loc';
end

%--------------------------------------------------------------------------
% Supervisedly trained classifier fixed length trials
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', capfile, 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'fix', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping margin
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', capfile, 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'margin', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping beta
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', capfile, 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'beta', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Supervisedly trained classifier dynamic stopping bayes
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.1;
cfg = struct( ...
    'verbosity', 3, 'n_folds', 5, 'user', subject, ...
    'capfile', capfile, 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'bes0', 'metric', 'inner', 'zerotraining', false, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Train classifier
tmp = data;
tmp.X = tmp.X(:, 1:floor(trial_time * fs), :);
jt_tmc_train(tmp, cfg);  % note: CV happening inside

%--------------------------------------------------------------------------
% Zero training classifier
%--------------------------------------------------------------------------

% Settings
trial_time = 4.2;
segment_time = 0.5;
cfg = struct( ...
    'verbosity', 0, 'n_folds', 5, 'user', subject, ...
    'capfile', capfile, 'fs', fs, ...
    'segment_time', segment_time, ...
    'min_time', segment_time, 'max_time', trial_time, ...
    'inter_trial_time', 1, 'delay', 0, 'latency_V', 0, 'latency_U', 0, ...
    'method', 'beta', 'metric', 'inner', 'zerotraining', true, ...
    'L', 0.3, 'event', 'duration', 'component', 1, 'model_onset', true, ...
    'lx', 0.9, 'ly', 'tukey', 'lx_amp', 0.1, 'ly_amp', 0.01, 'ly_perc', 0, ...
    'accuracy', 0.95, 'min_time_first', 10, 'accuracy_first', 0.99);

% Init classifier
tmp = struct();
tmp.X = [];
tmp.y = [];
tmp.V = data.U;
tmp.U = data.U;
classifier = jt_tmc_train(tmp, cfg);

% Zero training
n_trials = size(data.X, 3);
n_segments = floor(size(data.X, 2) / fs / segment_time);
accuracy = zeros(n_trials, 1);
duration = zeros(n_trials, 1);
for i = 1:n_trials
    for j = 1:n_segments
       
        % Apply classifier
        [label, ~, classifier] = jt_tmc_apply(classifier, ...
            data.X(:, 1:floor(j * segment_time * fs), i));
        
        % Save results if classified
        if ~isnan(label)
            accuracy(i) = label == data.y(i);
            duration(i) = j * segment_time;
            break;
        end
        
    end
end

% View classifier
classifier.accuracy.p = mean(accuracy);
classifier.accuracy.t = mean(duration);
classifier.accuracy.itr = jt_itr(size(data.V, 2), ...
    classifier.accuracy.p, classifier.accuracy.t);
jt_tmc_view(classifier);

% Visualize performance
figure();
subplot(2, 1, 1);
hold on;
plot(100 * accuracy);
plot([1 numel(accuracy)], 100 * [mean(accuracy) mean(accuracy)], '--k');
xlabel('trial [#]');
ylabel('accuracy [%]');
title(sprintf('average: %.1f%%', 100 * mean(accuracy)));
subplot(2, 1, 2);
hold on;
plot(duration);
plot([1 numel(duration)], [mean(duration) mean(duration)], '--k');
xlabel('trial [#]')
ylabel('duration [sec]');
title(sprintf('average: %.1f sec', mean(duration)));
sgtitle('Zero-training');
