%--------------------------------------------------------------------------
% README
%
% This script is a "general" script that can be used (adapted) to read raw
% data recorded by a TMSi Porti amplifier. It searches for the presented
% noise-codes and labels in the BrainStream saved files, and reads the EEG
% from the raw gdf file. Several standard preprocessing steps are
% performed. 
%
%--------------------------------------------------------------------------

% Restore workspace
restoredefaultpath;
clear variables;
close all;
clc;
rng(42);

%--------------------------------------------------------------------------
% Add libraries
%--------------------------------------------------------------------------

% Add Matlab Noise-Tagging library
addpath(genpath(fullfile('~', 'mant', 'mant')));

% Add FieldTrip library
addpath(fullfile('~', 'fieldtrip'));
ft_defaults;

%--------------------------------------------------------------------------
% Data path
%--------------------------------------------------------------------------

% Root to project data
root = fullfile('~', 'data', 'mant_ms');

%--------------------------------------------------------------------------
% Constants
%--------------------------------------------------------------------------

% Data files
subjects = {'pierre'};
sessions = {'20220412'};
blocks = {'train_1'};

fs = 120;  % target EEG sampling rate
fr = 60;  % monitor refresh rate
hpfreq = 2;  % high-pass cut-off frequency
lpfreq = 30;  % low-pass cut-off frequency
trial_time = 4.2;  % trial duration

%--------------------------------------------------------------------------
% Read data
%--------------------------------------------------------------------------

n_subjects = numel(subjects);
n_blocks = numel(blocks);

% Loop over subjects
for i_subject = 1:n_subjects
    subject = subjects{i_subject};
    session = sessions{i_subject};
    
    fprintf('\n\n\n---------------------------------------------\n');
    fprintf('\nsubject: %s\n\n', subject)

    % Pre-allocate data structure
    data = cell(n_blocks, 1);
    labels = cell(n_blocks, 1);
    codes = cell(n_blocks, 1);
    
    % Loop over blocks
    for i_block = 1:n_blocks
        block = blocks{i_block};

        % Read codes
        % Note, these are already repeated to the online EEG trialtime, 
        % so maight contain seveal repetitions!
        in = load(fullfile(root, subject, session, block, ...
            'main', 'brainstream', 'variables', 'stimuli.mat'));
        codes{i_block} = jt_upsample(in.v', floor(fs / fr));
        
        % Read labels
        in = load(fullfile(root, subject, session, block, ...
            'main', 'mdl_dataproc', 'brainstream', 'variables', ...
            'trainlabels.mat'));
        labels{i_block} = in.v(:);

        % Find data
        dataset = fullfile(root, subject, session, block, ...
            'main', 'raw_bdf', sprintf('%s_%s_%s_main_eeg.gdf', ...
            subject, session, block));
        
        % Read data
        cfg = [];
        cfg.dataset = dataset;
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg);
        data{i_block}.label{end} = 'STATUS';

        % Find trial onsets
        markers = find(data{i_block}.trial{1}(end, :) > 3);
        diffs = diff(markers);
        trials = [1 1 + find(diffs > 0.01 * data{i_block}.fsample)];
        onsets = markers(trials)';

%         % Inspect markers
%         figure();
%         hold on;
%         plot(data{i_block}.trial{1}(end, :), 'k-');
%         plot(markers, zeros(size(markers)), 'r.', 'markersize', 20);
%         plot(onsets, zeros(size(onsets)), 'g.', 'markersize', 20);
%         title(sprintf('%s | %s', subject, block));

        %------------------------------------------------------------------
        % High-pass
        cfg = [];
        cfg.hpfilter = 'yes';
        cfg.hpfreq = hpfreq;
        cfg.hpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});

        %------------------------------------------------------------------
        % Low-pass
        cfg = [];
        cfg.lpfilter = 'yes';
        cfg.lpfreq = lpfreq;
        cfg.lpfilttype = 'but';
        cfg.feedback = 'no';
        data{i_block} = ft_preprocessing(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Slice data
        % N.B. +1 second before and after for filter artefacts 
        cfg = [];
        cfg.dataset = dataset;
        prestim = onsets - data{i_block}.fsample;
        poststim = onsets + floor((trial_time + 1) * data{i_block}.fsample);
        cfg.trl = [prestim poststim zeros(numel(onsets), 1)];
        cfg.feedback = 'no';
        data{i_block} = ft_redefinetrial(cfg, data{i_block});
        
        %------------------------------------------------------------------
        % Select trials and channels
        % N.B.: remove STATUS/DIG channel (marker channel)
        cfg = []; 
        cfg.channel = 1:numel(data{i_block}.label)-1;
        cfg.feedback = 'no';
        data{i_block} = ft_selectdata(cfg, data{i_block});

        %------------------------------------------------------------------
        % Down-sample
        cfg = [];
        cfg.resamplefs = fs;
        cfg.feedback = 'no';
        data{i_block} = ft_resampledata(cfg, data{i_block});
        
        % Check data
        if numel(data{i_block}.trial) ~= 10
            error('Did not find 10 trials in this block!');
        end

    end

    % Append blocks
    data = ft_appenddata([], data{:});
    labels = cat(1, labels{:});
    codes = codes{1};
    
    % Extract data
    % N.B. remove 1 second before and after trial for filter artefacts
    n_samples = floor(trial_time * fs);
    X = cat(3, data.trial{:});
    X = X(:, fs + 1:fs + n_samples, :);
    
    % Change types
    X = double(X);
    y = uint8(labels);
    V = logical(codes);
    
    % Print sizes
    fprintf('\tX: %d x %d x %d \n', size(X, 1), size(X, 2), size(X, 3));
    fprintf('\ty: %d \n', numel(y));
    fprintf('\tV: %d x %d \n', size(V, 1), size(V, 2));
    
    % Create output folder
    if ~exist(fullfile(root, 'derivatives', subject), 'dir')
        mkdir(fullfile(root, 'derivatives', subject));
    end

    % Save data
    save(fullfile(root, 'derivatives', subject, ...
        sprintf('%s_gdf.mat', subject)), 'X', 'y', 'V', 'fs');
    
end
