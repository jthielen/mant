MaNT
====

MaNT is a Matlab library for the noise-tagging brain-computer interface (BCI) project developed at the Donders Institute for Brain, Cognition and Behaviour, Radboud University, Nijmegen, the Netherlands. It is a BCI that makes use of evoked responses, specifically the code-modulated visual evoked potential (c-VEP) of the electroencephalogram (EEG). 

Disclaimer
==========

This Matlab library is not maintained anymore, and as such deprecated. The development moved to Python, specifically the PyntBCI library: https://github.com/thijor/pyntbci.

Installation
============

To install MaNT from Matlab (i.e., add it to its path), use:

	addpath('mant')

Getting started
===============

Some example analysis pipelines including data loading, preprocessing and classification are provided in the `examples/` folder, which operate on the datasets as provided below. 

References
==========

* Thielen, J., van den Broek, P., Farquhar, J., & Desain, P. (2015). Broad-Band visually evoked potentials: re(con)volution in brain-computer interfacing. PloS one, 10(7), e0133797. DOI: [10.1371/journal.pone.0133797](https://doi.org/10.1371/journal.pone.0133797)
* Thielen, J., Marsman, P., Farquhar, J., & Desain, P. (2017). Re(con)volution: accurate response prediction for broad-band evoked potentials-based brain computer interfaces. In Brain-Computer Interface Research (pp. 35-42). Springer, Cham. DOI: [10.1007/978-3-319-64373-1_4](https://doi.org/10.1007/978-3-319-64373-1_4)
* Desain, P. W. M., Thielen, J., van den Broek, P. L. C., & Farquhar, J. D. R. (2019). U.S. Patent No. 10,314,508. Washington, DC: U.S. Patent and Trademark Office. Link: [here](https://patentimages.storage.googleapis.com/40/a3/bb/65db00c7de99ec/US10314508.pdf)
* Ahmadi, S., Borhanazad, M., Tump, D., Farquhar, J., & Desain, P. (2019). Low channel count montages using sensor tying for VEP-based BCI. Journal of neural engineering, 16(6), 066038. DOI: [10.1088/1741-2552/ab4057](https://doi.org/10.1088/1741-2552/ab4057)
* Thielen, J., Marsman, P., Farquhar, J., & Desain, P. (2021). From full calibration to zero training for a code-modulated visual evoked potentials for brain–computer interface. Journal of Neural Engineering, 18(5), 056007. DOI: [10.1088/1741-2552/abecef](https://doi.org/10.1088/1741-2552/abecef)
* Verbaarschot, C., Tump, D., Lutu, A., Borhanazad, M., Thielen, J., van den Broek, P., ... & Desain, P. (2021). A visual brain-computer interface as communication aid for patients with amyotrophic lateral sclerosis. Clinical Neurophysiology, 132(10), 2404-2415. DOI: [10.1016/j.clinph.2021.07.012](https://doi.org/10.1016/j.clinph.2021.07.012)
* Martínez-Cagigal, V., Thielen, J., Santamaría-Vázquez, E., Pérez-Velasco, S., Desain, P., & Hornero, R. (2021). Brain–computer interfaces based on code-modulated visual evoked potentials (c-VEP): a literature review. Journal of Neural Engineering. DOI: [10.1088/1741-2552/ac38cf](https://doi.org/10.1088/1741-2552/ac38cf)

Datasets
========

* Thielen et al. (2018) Broad-Band Visually Evoked Potentials: Re(con)volution in Brain-Computer Interfacing. DOI: [10.34973/1ecz-1232](https://doi.org/10.34973/1ecz-1232)
* Ahmadi et al. (2018) High density EEG measurment. DOI: [10.34973/psaf-mq72](https://doi.org/10.34973/psaf-mq72)
* Ahmadi et al. (2019) Sensor tying. [10.34973/ehq6-b836](https://doi.org/10.34973/ehq6-b836)
* Thielen et al. (2021) From full calibration to zero training for a code-modulated visual evoked potentials brain computer interface. [10.34973/9txv-z787](https://doi.org/10.34973/9txv-z787)

Contact
=======

* Jordy Thielen (jordy.thielen@donders.ru.nl)
